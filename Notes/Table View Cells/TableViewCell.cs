﻿using System;

using UIKit;

namespace Notes
{
	public abstract class TableViewCell : UITableViewCell
	{
		public UIView CellContent { get; set; }

		protected abstract UIView CreateCellContentView ();

		public TableViewCell (IntPtr ptr) : base (ptr)
		{
			UIView contentView = this.CreateCellContentView ();
			this.ContentView.AddConstraintsSubview (contentView);

			this.AddContentViewConstraints (contentView);

			this.CellContent = contentView;
		}

		protected virtual void AddContentViewConstraints (UIView contentView)
		{
			this.ContentView.AlignViews (contentView);
		}
	}
}

