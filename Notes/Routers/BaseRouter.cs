﻿using System;

namespace Notes
{
	public class BaseRouter : IRouter
	{
		protected BaseViewController _controller = null;

		public BaseRouter (BaseViewController controller)
		{
			_controller = controller;
		}
	}
}

