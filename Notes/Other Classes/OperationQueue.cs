﻿using System;
using System.Threading.Tasks;
using System.Threading;

namespace Notes
{
	public class OperationQueue
	{
		TaskFactory _taskFactory = new TaskFactory (
			TaskCreationOptions.PreferFairness, 
			TaskContinuationOptions.PreferFairness
		);
		Task _lastTask = null;

		public Task AddTask (Action action)
		{
			return this.AddTask (action, CancellationToken.None);
		}

		public Task AddTask (Action action, CancellationToken token)
		{
			Action operation = () => {
				try {
					action ();
				} catch (OperationCanceledException) {
					throw;
				} catch (Exception) {
					Console.WriteLine ("Fatal Exception in Operation Queue");
				}
			};
			if (_lastTask == null) {
				_lastTask = _taskFactory.StartNew (operation);
			} else {
				_lastTask = _taskFactory.ContinueWhenAll (new Task[] { _lastTask }, completedTasks => {
					operation ();
				}, token);
			}
			return _lastTask;
		}
	}
}

