﻿using System;

using Foundation;
using UIKit;

namespace Notes
{
	public class GlobalDataContext
	{
		static GlobalDataContext _instance = new GlobalDataContext ();

		public static GlobalDataContext SharedInstance ()
		{
			return _instance;
		}

		int _activityIndicatorCounter = 0;
		public int ActivityIndicatorCounter { 
			get { lock (_activityIndicatorLock) { return _activityIndicatorCounter; } } 
			set { 
				lock (_activityIndicatorLock) {
					_activityIndicatorCounter = value;
					if (this.ActivityIndicatorEvent != null) this.ActivityIndicatorEvent (this);
				}
			} 
		}
		public event PropertyDidChangeHandler ActivityIndicatorEvent;
		Object _activityIndicatorLock = new object ();

		public void IncreaseActivityIndicatorCounter ()
		{
			lock (_activityIndicatorLock) {
				this.ActivityIndicatorCounter++;
			}
		}

		public void DecreaseActivityIndicatorCounter ()
		{
			lock (_activityIndicatorLock) {
				if (this.ActivityIndicatorCounter > 0) {
					this.ActivityIndicatorCounter--;
				}
			}
		}
	}
}

