﻿using System;

using UIKit;
using Foundation;

namespace Notes
{
	public class NetworkActivityIndicatorController
	{
		public NetworkActivityIndicatorController ()
		{
			GlobalDataContext.SharedInstance ().ActivityIndicatorEvent += CheckActivityIndicatorCounter;
		}

		void CheckActivityIndicatorCounter (Object sender)
		{
			GlobalDataContext dataContext = sender as GlobalDataContext;
			int counter = dataContext.ActivityIndicatorCounter;
			UIApplication application = UIApplication.SharedApplication;
			application.BeginInvokeOnMainThread (() => {
				application.NetworkActivityIndicatorVisible = counter > 0;
			});
		}
	}
}

