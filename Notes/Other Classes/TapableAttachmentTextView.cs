﻿using System;

using Foundation;
using UIKit;
using CoreGraphics;
using ObjCRuntime;

namespace Notes
{
	public class TapableAttachmentTextView : UITextView
	{
		const int kInvalidIndex = -1;
		int _attachmentCharacterIndex = kInvalidIndex;
		public int AttachmentCharacterIndex { get { return _attachmentCharacterIndex; } }
		public event PropertyDidChangeHandler AttachmentCharacterIndexEvent;

		public override bool GestureRecognizerShouldBegin (UIGestureRecognizer gestureRecognizer) 
		{
			if (this.SelectedRange.Length == 0 && gestureRecognizer.IsKindOfClass (new UITapGestureRecognizer ().Class)) {
				UITapGestureRecognizer tapRecognizer = (UITapGestureRecognizer)gestureRecognizer;
				if (tapRecognizer.NumberOfTouchesRequired == 1 && tapRecognizer.NumberOfTapsRequired == 1) {
					return !this.IsTappedMediaAttachment (gestureRecognizer.LocationInView (this));
				}
			}
			return base.GestureRecognizerShouldBegin (gestureRecognizer);
		}

		bool IsTappedMediaAttachment (CGPoint point) 
		{
			if (this.AttributedText.Length == 0) {
				return false;
			}

			NSTextContainer textContainer = this.TextContainer;
			NSLayoutManager layoutManager = this.LayoutManager;

			point.X -= this.TextContainerInset.Left;
			point.Y -= this.TextContainerInset.Top;

			nfloat partialFraction = 0;
			nuint characterIndex = layoutManager.CharacterIndexForPoint (
				point,
				textContainer,
				ref partialFraction
			);
			if ((nint)characterIndex >= this.AttributedText.Length) {
				return false;
			}

			NSRange range;
			NSTextAttachment attachment = (NSTextAttachment)this.AttributedText.GetAttribute (
				UIStringAttributeKey.Attachment,
				(nint)characterIndex,
				out range
			);
			if (attachment != null) {
				_attachmentCharacterIndex = (int)characterIndex;
				if (this.AttachmentCharacterIndexEvent != null) {
					this.AttachmentCharacterIndexEvent (this);
				}
				_attachmentCharacterIndex = kInvalidIndex;
			}
			return true;
		}
	}
}

