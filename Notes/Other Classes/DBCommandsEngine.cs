﻿using System;
using System.Data;
using System.IO;
using Mono.Data.Sqlite;

namespace Notes
{
	public delegate void QueryCompletion (IDataReader reader);
	public delegate void DbExistCompletion ();
	public delegate void DbNotExistCompletion ();
	public delegate void ConnectionCompletion (IDbConnection connection);

	public class DBCommandsEngine
	{
		static string _dbName = Path.Combine (Constants.Documents, "mydb.db3");

		static DBCommandsEngine _instance = new DBCommandsEngine ();

		public static DBCommandsEngine SharedInstance ()
		{
			return _instance;
		}

		public void DbExists (DbExistCompletion existCompletion, DbNotExistCompletion notExistCompletion)
		{
			bool exists = File.Exists (_dbName);
			if (!exists) {
				SqliteConnection.CreateFile (_dbName);
			}

			if (exists) {
				existCompletion ();
			} else {
				notExistCompletion ();
			}
		}

		public void ExecuteTransaction (ConnectionCompletion body)
		{
			this.GetConnection (connection => {
				using (var transaction = connection.BeginTransaction ()) {
					body (connection);
					transaction.Commit ();
				}
			});
		}

		public void ExecuteQuery (string statement, QueryCompletion completion)
		{
			IDataReader reader = null;
			this.Execute (statement, command => {
				reader = command.ExecuteReader ();
				completion (reader);
			});
		}

		public int ExecuteNonQuery (string statement)
		{
			int result = 0;
			this.Execute (statement, command => {
				result = command.ExecuteNonQuery ();
			});
			return result;
		}

		public void ExecuteQuery (IDbConnection connection, string statement, QueryCompletion completion)
		{
			IDataReader reader = null;
			this.CreateCommand (connection, statement, command => {
				reader = command.ExecuteReader ();
				completion (reader);
			});
		}

		public int ExecuteNonQuery (IDbConnection connection, string statement)
		{
			int result = 0;
			this.CreateCommand (connection, statement, command => {
				result = command.ExecuteNonQuery ();
			});
			return result;
		}

		void GetConnection (ConnectionCompletion completion)
		{
			using (var connection = new SqliteConnection("Data Source=" + _dbName)) {
				connection.Open ();
				completion (connection);
				connection.Close ();
			}
		}

		void CreateCommand (IDbConnection connection, string statement, Action<IDbCommand> action)
		{
			using (var command = connection.CreateCommand ()) {
				command.CommandText = statement;
				action (command);
			}
		}

		void Execute (string statement, Action<IDbCommand> action)
		{
			this.GetConnection (connection => {
				this.CreateCommand (connection, statement, action);
			});
		}
	}
}

