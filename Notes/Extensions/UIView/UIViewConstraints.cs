﻿using System;

using UIKit;
using CoreGraphics;
using ObjCRuntime;

namespace Notes
{
	public static class UIViewConstraints
	{
		public static void AddConstraintsSubview (this UIView superView, UIView subview)
		{
			subview.TranslatesAutoresizingMaskIntoConstraints = false;
			superView.AddSubview (subview);
		}

		public static NSLayoutConstraint AddConstraintMargin (
			this UIView superview,
			NSLayoutAttribute argumentAttribute,
			UIView functionView
		)
		{
			return superview.AddConstraintMargin (argumentAttribute, 0f, functionView);
		}

		public static NSLayoutConstraint AddConstraintMargin (
			this UIView superview,
			NSLayoutAttribute argumentAttribute,
			nfloat margin,
			UIView functionView
		)
		{
			return superview.AddConstraintMargin (argumentAttribute, 1f, functionView, argumentAttribute);
		}

		public static NSLayoutConstraint AddConstraintMargin (
			this UIView superview,
			NSLayoutAttribute argumentAttribute,
			nfloat margin,
			UIView functionView,
			NSLayoutAttribute functionAttribute
		)
		{
			return superview.AddConstraintMargin (superview, argumentAttribute, margin, functionView, functionAttribute);
		}

		public static NSLayoutConstraint AddConstraintMargin (
			this UIView superview,
			UIView argumentView,
			NSLayoutAttribute argumentAttribute,
			UIView functionView,
			NSLayoutAttribute functionAttribute
		)
		{
			return superview.AddConstraintMargin (argumentView, argumentAttribute, 0f, functionView, functionAttribute);
		}

		public static NSLayoutConstraint AddConstraintMargin (
			this UIView superview,
			IUILayoutSupport argumentGuide,
			NSLayoutAttribute argumentAttribute,
			UIView functionView,
			NSLayoutAttribute functionAttribute
		)
		{
			return superview.AddConstraintMargin (argumentGuide, argumentAttribute, 0f, functionView, functionAttribute);
		}

		public static NSLayoutConstraint AddConstraintMargin (
			this UIView superview,
			UIView argumentView,
			NSLayoutAttribute argumentAttribute,
			nfloat margin,
			UIView functionView,
			NSLayoutAttribute functionAttribute
		)
		{
			return superview.AddConstraintMargin (1f, argumentView, argumentAttribute, margin, NSLayoutRelation.Equal, functionView, functionAttribute);
		}

		public static NSLayoutConstraint AddConstraintMargin (
			this UIView superview,
			IUILayoutSupport argumentGuide,
			NSLayoutAttribute argumentAttribute,
			nfloat margin,
			UIView functionView,
			NSLayoutAttribute functionAttribute
		)
		{
			return superview.AddConstraintMargin (1f, argumentGuide, argumentAttribute, margin, NSLayoutRelation.Equal, functionView, functionAttribute);
		}

		public static NSLayoutConstraint AddConstraintMargin (
			this UIView superview,
			nfloat elasticityMultiplier,
			UIView argumentView,
			NSLayoutAttribute argumentAttribute,
			UIView functionView,
			NSLayoutAttribute functionAttribute
		)
		{
			return superview.AddConstraintMargin (elasticityMultiplier, argumentView, argumentAttribute, 0f, NSLayoutRelation.Equal, functionView, functionAttribute);
		}

		public static NSLayoutConstraint AddConstraintMargin (
			this UIView superview,
			nfloat elasticityMultiplier,
			UIView argumentView,
			NSLayoutAttribute argumentAttribute,
			nfloat margin,
			NSLayoutRelation relation,
			UIView functionView,
			NSLayoutAttribute functionAttribute
		)
		{
			CGPoint startPoint = argumentView.ConvertPointToView (new CGPoint (0, 0), superview);
			nfloat startCoordinate = startPoint.X;
			if (
				argumentAttribute == NSLayoutAttribute.Top ||
				argumentAttribute == NSLayoutAttribute.TopMargin ||
				argumentAttribute == NSLayoutAttribute.Bottom ||
				argumentAttribute == NSLayoutAttribute.BottomMargin
			) {
				startCoordinate = startPoint.Y;
			} 
			return superview.AddConstraintMargin (
				elasticityMultiplier, argumentView, argumentAttribute, margin, relation, functionView, functionAttribute, startCoordinate
			);
		}

		public static NSLayoutConstraint AddConstraintMargin (
			this UIView superview,
			nfloat elasticityMultiplier,
			IUILayoutSupport argumentGuide,
			NSLayoutAttribute argumentAttribute,
			nfloat margin,
			NSLayoutRelation relation,
			UIView functionView,
			NSLayoutAttribute functionAttribute
		)
		{
			nfloat startCoordinate = argumentGuide.Length;
			return superview.AddConstraintMargin (
				elasticityMultiplier, argumentGuide, argumentAttribute, margin, relation, functionView, functionAttribute, startCoordinate
			);
		}

		public static NSLayoutConstraint AddConstraintMargin (
			this UIView superview,
			nfloat elasticityMultiplier,
			INativeObject argument,
			NSLayoutAttribute argumentAttribute,
			nfloat margin,
			NSLayoutRelation relation,
			UIView functionView,
			NSLayoutAttribute functionAttribute,
			nfloat startCoordinate
		)
		{
			nfloat autolayoutMultiplier = elasticityMultiplier, autolayoutConstant = margin;
			if (
				argumentAttribute == NSLayoutAttribute.Leading ||
				argumentAttribute == NSLayoutAttribute.LeadingMargin ||
				argumentAttribute == NSLayoutAttribute.Left ||
				argumentAttribute == NSLayoutAttribute.LeftMargin ||
				argumentAttribute == NSLayoutAttribute.Top ||
				argumentAttribute == NSLayoutAttribute.TopMargin
			) {
				autolayoutConstant = -autolayoutConstant;
				autolayoutMultiplier = 1 / autolayoutMultiplier;
			}
			autolayoutConstant += startCoordinate;
			return superview.AddConstraint (
				elasticityMultiplier, argument, argumentAttribute, margin, relation, functionView, functionAttribute
			);
		}

		public static NSLayoutConstraint AddConstraint (
			this UIView superview,
			nfloat elasticityMultiplier,
			INativeObject argument,
			NSLayoutAttribute argumentAttribute,
			nfloat margin,
			NSLayoutRelation relation,
			UIView functionView,
			NSLayoutAttribute functionAttribute
		)
		{
			NSLayoutConstraint constraint = NSLayoutConstraint.Create (
				                                functionView, functionAttribute, relation, argument, argumentAttribute, elasticityMultiplier, margin
			                                );
			superview.AddConstraint (
				constraint
			);
			return constraint;
		}

		public static NSLayoutConstraint AddHeight (this UIView view, nfloat height)
		{
			NSLayoutConstraint constraint = NSLayoutConstraint.Create (view, NSLayoutAttribute.Height, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1.0f, height);
			view.AddConstraint (constraint);
			return constraint;
		}

		public static NSLayoutConstraint AddWidth (this UIView view, nfloat width)
		{
			NSLayoutConstraint constraint = NSLayoutConstraint.Create (view, NSLayoutAttribute.Width, NSLayoutRelation.Equal, null, NSLayoutAttribute.NoAttribute, 1.0f, width);
			view.AddConstraint (constraint);
			return constraint;
		}

		public static NSLayoutConstraint AddCenterX (this UIView superView, UIView secondView)
		{
			return superView.AddCenterX (superView, secondView, 0.0f);
		}

		public static NSLayoutConstraint AddCenterX (this UIView superView, UIView secondView, nfloat x)
		{
			return superView.AddCenterX (superView, secondView, x);
		}

		public static NSLayoutConstraint AddCenterX (this UIView superView, UIView secondView, nfloat multiplier, nfloat x)
		{
			return superView.AddCenterX (superView, secondView, multiplier, x);
		}

		public static NSLayoutConstraint AddCenterX (this UIView superView, UIView firstView, UIView secondView, nfloat x)
		{
			return superView.AddCenterX (firstView, secondView, 1f, x);
		}

		public static NSLayoutConstraint AddCenterX (this UIView superView, UIView firstView, UIView secondView, nfloat multiplier, nfloat x)
		{
			NSLayoutConstraint constraint = NSLayoutConstraint.Create (firstView, NSLayoutAttribute.CenterX, NSLayoutRelation.Equal, secondView, NSLayoutAttribute.CenterX, multiplier, x);
			superView.AddConstraint (constraint);
			return constraint;
		}

		public static NSLayoutConstraint AddCenterY (this UIView superView, UIView secondView)
		{
			return superView.AddCenterY (superView, secondView, 0.0f);
		}

		public static NSLayoutConstraint AddCenterY (this UIView superView, UIView secondView, nfloat y)
		{
			return superView.AddCenterX (superView, secondView, y);
		}

		public static NSLayoutConstraint AddCenterY (this UIView superView, UIView secondView, nfloat multiplier, nfloat y)
		{
			return superView.AddCenterX (superView, secondView, multiplier, y);
		}

		public static NSLayoutConstraint AddCenterY (this UIView superView, UIView firstView, UIView secondView, nfloat y)
		{
			return superView.AddCenterY (firstView, secondView, 1f, y);
		}

		public static NSLayoutConstraint AddCenterY (this UIView superView, UIView firstView, UIView secondView, nfloat multiplier, nfloat y)
		{
			NSLayoutConstraint constraint = NSLayoutConstraint.Create (firstView, NSLayoutAttribute.CenterY, NSLayoutRelation.Equal, secondView, NSLayoutAttribute.CenterY, multiplier, y);
			superView.AddConstraint (constraint);
			return constraint;
		}

		public static void AlignViews (this UIView superView, UIView subview)
		{
			superView.AlignViews (subview, 0f);
		}

		public static void AlignViews (this UIView superView, UIView subview, nfloat constant)
		{
			superView.AlignViews (superView, subview, constant);
		}

		public static void AlignViews (this UIView superView, UIView firstView, UIView secondView)
		{
			superView.AlignViews (firstView, secondView, 0f);
		}

		public static void AlignViews (this UIView superView, UIView firstView, UIView secondView, nfloat constant)
		{
			superView.AlignViews (firstView, secondView, constant, constant, constant, constant);
		}

		public static void AlignViews (this UIView superView, UIView firstView, UIView secondView, nfloat leading, nfloat trailing, nfloat top, nfloat bottom)
		{
			superView.AlignVertical (firstView, secondView, top, bottom);
			superView.AlignHorizontal (firstView, secondView, leading, trailing);
		}

		public static void AlignVertical (this UIView superView, UIView firstView, UIView secondView, nfloat top, nfloat bottom)
		{
			superView.AddConstraint (NSLayoutConstraint.Create (firstView, NSLayoutAttribute.Top, NSLayoutRelation.Equal, secondView, NSLayoutAttribute.Top, 1f, top));
			superView.AddConstraint (NSLayoutConstraint.Create (firstView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, secondView, NSLayoutAttribute.Bottom, 1f, bottom));
		}

		public static void AlignHorizontal (this UIView superView, UIView firstView, UIView secondView, nfloat leading, nfloat trailing)
		{
			superView.AddConstraint (NSLayoutConstraint.Create (firstView, NSLayoutAttribute.Leading, NSLayoutRelation.Equal, secondView, NSLayoutAttribute.Leading, 1f, leading));
			superView.AddConstraint (NSLayoutConstraint.Create (firstView, NSLayoutAttribute.Trailing, NSLayoutRelation.Equal, secondView, NSLayoutAttribute.Trailing, 1f, trailing));
		}

		public static void ChainViewsRight (this UIView superView, UIView firstView, UIView secondView)
		{
			superView.ChainViewsRight (firstView, secondView, 0f, 0f, 0f);
		}

		public static void ChainViewsRight (this UIView superView, UIView firstView, UIView secondView, nfloat margin)
		{
			superView.ChainViewsRight (firstView, secondView, margin, 0f, 0f);
		}

		public static void ChainViewsRight (this UIView superView, UIView firstView, UIView secondView, nfloat margin, nfloat top, nfloat bottom)
		{
			superView.AddConstraint (NSLayoutConstraint.Create (firstView, NSLayoutAttribute.Trailing, NSLayoutRelation.Equal, secondView, NSLayoutAttribute.Leading, 1f, margin));
			superView.AlignVertical (firstView, secondView, top, bottom);
		}

		public static void ChainViewsBottom (this UIView superView, UIView firstView, UIView secondView)
		{
			superView.ChainViewsBottom (firstView, secondView, 0f, 0f, 0f);
		}

		public static void ChainViewsBottom (this UIView superView, UIView firstView, UIView secondView, nfloat margin)
		{
			superView.ChainViewsBottom (firstView, secondView, margin, 0f, 0f);
		}

		public static void ChainViewsBottom (this UIView superView, UIView firstView, UIView secondView, nfloat margin, nfloat leading, nfloat trailing)
		{
			superView.AddConstraint (NSLayoutConstraint.Create (firstView, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, secondView, NSLayoutAttribute.Top, 1f, margin));
			superView.AlignHorizontal (firstView, secondView, leading, trailing);
		}
	}
}

