﻿using System;

using Foundation;
using UIKit;

namespace Notes
{
	public static class ToolbarExtension
	{
		public static UIToolbar CreateToolbar (this UIViewController viewController, string actionName, EventHandler action)
		{
			UIToolbar toolbar = new UIToolbar ();
			viewController.View.AddConstraintsSubview (toolbar);
			toolbar.BackgroundColor = Constants.MainBackgroundColor;
			toolbar.BarTintColor = Constants.MainBackgroundColor;
			toolbar.TintColor = Constants.MainTintColor;
			toolbar.Translucent = false;
			UIBarButtonItem actionButton = new UIBarButtonItem (
				NSBundle.MainBundle.LocalizedString (actionName, null),
				UIBarButtonItemStyle.Bordered,
				action
			);
			UIBarButtonItem space = new UIBarButtonItem (UIBarButtonSystemItem.FlexibleSpace);
			toolbar.SetItems (new [] { space, actionButton }, false);
			return toolbar;
		}
	}
}

