﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Notes
{
	public abstract class DBObject
	{
		public delegate T CreateObjectHandler<T> (IDataReader reader);

		public const string kIDKey = "id";
		public const string kSelected = "selected";

		public int ID { get; set; }

		protected int _selected = 0;
		public bool Selected { 
			get { return _selected == 0 ? false : true; } 
			set { 
				if (value) {
					_selected = 1;
				} else {
					_selected = 0;
				}
			} 
		}

		public abstract string InsertStatement ();
		public abstract string UpdateStatement ();

		public DBObject ()
		{
		}

		public static bool Save (string[] statements)
		{
			bool transactionResult = false, failed = false;
			DBCommandsEngine engine = DBCommandsEngine.SharedInstance ();
			engine.ExecuteTransaction (connection => {
				foreach (var statement in statements) {
					if (engine.ExecuteNonQuery (connection, statement) == Constants.NonQueryFailed) {
						Console.WriteLine ("User not saved");
						transactionResult = false;
						failed = true;
					} else if (!failed) {
						transactionResult = true;
					}						
				}
			});
			return transactionResult;
		}

		protected static List<T> AllObjects<T> (string statement, CreateObjectHandler<T> handler)
		{
			List<T> objects = new List<T> ();
			DBCommandsEngine.SharedInstance ().ExecuteQuery (statement, reader => {
				while (reader.Read ()) {
					T newObject = handler (reader);
					objects.Add (newObject);
				}
			});
			return objects;
		}
	}
}

