﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Json;

namespace Notes
{
	public class DBNote : DBObject
	{
		const string kNotes = "notes";
		const string kContent = "content";
		const string kLastEdit = "last_edit";
		const string kFolderID = "folder_id";

		public const string kVideoCode = "vid";
		public const string kImageCode = "img";
		public const string kTextCode = "txt";

		public const int kTypeLength = 5;//txt//
		public const int kTypeCodeLength = 3;//vid, img, txt

		public List<string> Content { get; set; } = null;
		public DateTime LastEdit { get; set; } = default (DateTime);
		public int FolderID { get; set; } = -1;

		static DBNote ()
		{
			string format = 
				"CREATE TABLE IF NOT EXISTS {0} " +
				"({1} INTEGER PRIMARY KEY AUTOINCREMENT, {2} INT REFERENCES {3} ({4}) ON DELETE CASCADE, {5} TEXT, {6} TEXT, {7} INT)";
			string statement = string.Format (format, kNotes, kIDKey, kFolderID, DBFolder.kFolders, kIDKey, kContent, kLastEdit, kSelected);
			DBCommandsEngine.SharedInstance ().ExecuteNonQuery (statement);
		}

		public static List<DBNote> AllNotes (int folderID)
		{
			return DBObject.AllObjects (string.Format ("SELECT * FROM {0} WHERE {1} == {2}", kNotes, kFolderID, folderID), reader => {
				DBNote newNote = new DBNote ();
				newNote.ID = reader.GetInt32 (0);
				string content = (string)reader [kContent];
				JsonArray json = JsonValue.Parse (content) as JsonArray;
				List<string> jsonContent = new List<string> ();
				foreach (string item in json) {
					jsonContent.Add (item);
				}
				newNote.Content = jsonContent;
				newNote.LastEdit = DateTime.Parse ((string)reader [kLastEdit]);
				newNote._selected = (int)reader [kSelected];
				newNote.FolderID = (int)reader [kFolderID];
				return newNote;
			});
		}

		public DBNote () : base () {}

		public DBNote (int folder) : base ()
		{
			this.Content = new List<string> ();
			this.LastEdit = DateTime.Now;

			this.FolderID = folder;
		}

		public override string UpdateStatement ()
		{
			return string.Format (
				"UPDATE {0} SET {1} = {2}, {3} = '{4}', {5} = '{6}', {7} = {8} WHERE {9} == {10}",
				kNotes, kFolderID, this.FolderID, kContent, this.ContentJson ().ToString (), kLastEdit, this.LastEditString (), kSelected, _selected, kIDKey, this.ID
			);
		}

		public override string InsertStatement ()
		{
			JsonValue json = this.ContentJson ();
			string statement = string.Format (
				"INSERT OR REPLACE INTO {0} ({1}, {2}, {3}, {4}) VALUES ({5}, '{6}', '{7}', {8})",
				kNotes, kFolderID, kContent, kLastEdit, kSelected, this.FolderID, json.ToString (), this.LastEditString (), _selected
			);
			return statement;
		}

		public bool UpdateNote ()
		{
			return DBNote.Save (new [] {this.UpdateStatement ()});
		}

		public bool InsertNote ()
		{
			return DBNote.Save (new [] {this.InsertStatement ()});
		}

		public string DeleteStatement ()
		{
			string statement = string.Format (
				"DELETE FROM {0} " +
				"WHERE {1} == {2}",
				kNotes, kIDKey, this.ID
			);
			return statement;
		}

		string LastEditString ()
		{
			return this.LastEdit.ToString ("yyyy-MM-dd HH:mm:ss.fff zzz");
		}

		JsonValue ContentJson ()
		{
			List<JsonPrimitive> jsonContent = new List<JsonPrimitive> ();
			foreach (string item in this.Content) {
				jsonContent.Add (new JsonPrimitive (item));
			}
			JsonArray json = new JsonArray (jsonContent.ToArray ());
			return json;
		}
	}
}

