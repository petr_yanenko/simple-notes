﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Notes
{
	public class DBFolder : DBObject
	{
		public const string kFolders = "folders";
		const string kFolderTitle = "title";
		const string kCount = "count";

		public string Title { get; set; } = null;
		public int Count { get; set; } = 0;

		List<DBNote> _notes = null;
		public List<DBNote> Notes { 
			get { 
				if (_notes == null) {
					_notes = DBNote.AllNotes (this.ID);
				}
				return _notes;
			}
		}

		static DBFolder ()
		{
			string format = 
				"CREATE TABLE IF NOT EXISTS {0} " +
				"({1} INTEGER PRIMARY KEY AUTOINCREMENT, {2} TEXT, {3} INT, {4} INT)";
			string statement = string.Format (format, kFolders, kIDKey, kFolderTitle, kCount, kSelected);
			DBCommandsEngine.SharedInstance ().ExecuteNonQuery (statement);
		}

		public DBFolder (string title) : base ()
		{
			this.Title = title;
		}

		public static List<DBFolder> Folders (int[] excluded)
		{
			string whereClause = "";
			if (excluded != null && excluded.Length > 0) {
				string idsString = "";
				foreach (int item in excluded) {
					if (item != excluded [excluded.Length - 1]) {
						idsString += string.Format ("{0}, ", item);
					} else {
						idsString += string.Format ("{0}", item);
					}
				}
				whereClause = string.Format ("WHERE id NOT IN ({0})", idsString);
			}
			return DBObject.AllObjects (string.Format ("SELECT * FROM {0} {1}", kFolders, whereClause), reader => {
				DBFolder newFolder = new DBFolder (null);
				newFolder.ID = reader.GetInt32 (0);
				newFolder.Title = (string)reader [kFolderTitle];
				newFolder.Count = (int)reader [kCount];
				newFolder._selected = (int)reader [kSelected];
				return newFolder;
			});
		}

		public void ResetNotes ()
		{
			_notes = null;
		}

		public override string UpdateStatement ()
		{
			return string.Format (
				"UPDATE {0} SET {1} = '{2}', {3} = {4}, {5} = {6} WHERE {7} == {8}",
				kFolders, kFolderTitle, this.Title, kCount, this.Count, kSelected, _selected, kIDKey, this.ID
			);
		}

		public bool UpdateFolder ()
		{
			return DBFolder.Save (new [] { this.UpdateStatement () });
		}

		public bool InsertFolder ()
		{
			string statement = this.InsertStatement ();

			return DBFolder.Save (new [] {statement});
		}

		public bool DeleteFolder ()
		{
			string statement = string.Format (
				"DELETE FROM {0} " +
				"WHERE {1} == {2}",
				kFolders, kIDKey, this.ID
			);

			return DBFolder.Save (new [] {statement});
		}

		public override string InsertStatement ()
		{
			return string.Format (
				"INSERT OR REPLACE INTO {0} ({1}, {2}, {3}) VALUES ('{4}', {5}, {6})",
				kFolders, kFolderTitle, kCount, kSelected, this.Title, this.Count, _selected
			);
		}
	}
}

