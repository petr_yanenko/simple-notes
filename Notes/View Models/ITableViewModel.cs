﻿using System;

using Foundation;

namespace Notes
{
	public interface ITableViewModel
	{
		int SectionsNumber ();
		int CellsNumber (nint section);
		string CellReuseIdentifier (NSIndexPath indexPath);
		nfloat RowHeight (nfloat tableHeight, NSIndexPath indexPath);
		void SelectRowAction (NSIndexPath indexPath);
	}
}

