﻿using System;
using System.Threading;

namespace Notes
{
	public abstract class IOSViewModel<T> : BaseViewModel<T>, IiOSViewModel
	{
		int _guiTasksCount = 0;
		object _tasksCountLock = new object ();
		CancellationTokenSource _cancelSource = new CancellationTokenSource ();
		CancellationToken _token;

		bool _disableGUITasks = false;

		public abstract string NavigationTitle { get; }

		public IOSViewModel (IModel model) : base (model) 
		{
			_token = _cancelSource.Token;
		}

		protected override void DisableGUITasks ()
		{
			lock (_tasksCountLock) {
				_disableGUITasks = true;
			}
		}

		protected override void AddGUITask (Action action)
		{
			lock (_tasksCountLock) {
				if (_disableGUITasks) {
					return;
				}
				_guiTasksCount++;
				_cancelSource.Cancel ();
				CancellationTokenSource disposedSource = _cancelSource;
				Queue.ApplicationQueue.AddTask (() => {
					disposedSource.Dispose ();
				});
				_cancelSource = new CancellationTokenSource ();
				_token = _cancelSource.Token;
			}
			CoreFoundation.DispatchQueue.MainQueue.DispatchAsync (() => {
				lock (_tasksCountLock) {
					_guiTasksCount--;
					if (_disableGUITasks) {
						return;
					}
				}
				action ();
			});
		}

		protected override void AddApplicationTask (Action action)
		{
			lock (_tasksCountLock) {
				if (_guiTasksCount == 0 || _disableGUITasks) {
					Queue.ApplicationQueue.AddTask (action, _token);
				} else {
					Console.WriteLine ("Application task was declined");
				}
			}
		}
	}
}

