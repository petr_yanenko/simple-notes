﻿using System;

using Foundation;
using UIKit;

namespace Notes
{
	public abstract class EditableTableViewModel<T> : TableViewModel<T>, IEditableTableViewModel
		where T : SubtitleCellModel, new ()
	{
		protected NotesModel Model { get { return (NotesModel)_model; } }

		protected NSIndexPath _deletedPath = null;
		public NSIndexPath DeletedPath { get { return _deletedPath; } }
		public event PropertyDidChangeHandler DeletedPathEvent;

		protected int _selectedRowIndex = 0;
		public NSIndexPath SelectedRowPath { get { return NSIndexPath.FromRowSection (_selectedRowIndex, 0); } }
		public event PropertyDidChangeHandler SelectRowEvent;
		public event PropertyDidChangeHandler HighlightSelectedRowEvent;

		protected int _fromIndex = 0;
		public NSIndexPath FromIndexPath { get { return NSIndexPath.FromRowSection (_fromIndex, 0); } }
		public NSIndexPath ToIndexPath { get { return NSIndexPath.FromRowSection (0, 0); } }
		public event PropertyDidChangeHandler MoveRowEvent;

		protected int _reloadIndex = 0;
		public NSIndexPath ReloadIndexPath { get { return NSIndexPath.FromRowSection (_reloadIndex, 0); } }
		public event PropertyDidChangeHandler ReloadRowEvent;

		public override string NavigationTitle {
			get { return NSBundle.MainBundle.LocalizedString ("Notes", null); }
		}

		protected abstract void SelectItem (int id);
		protected abstract void DeleteItem (int id);

		public EditableTableViewModel (IModel model) : base (model)
		{
		}

		public override string CellReuseIdentifier (NSIndexPath indexPath)
		{
			return "DefaultCell";
		}

		public override nfloat RowHeight (nfloat tableHeight, NSIndexPath indexPath)
		{
			return 44f;
		}

		public virtual void DeleteRowAction (NSIndexPath indexPath)
		{
			SubtitleCellModel cellModel = _data [indexPath.Row];
			_data.RemoveAt (indexPath.Row);
			_deletedPath = indexPath;
			this.SendEvent (this.DeletedPathEvent);
			_deletedPath = null;
			int id = cellModel.ID;
			this.AddApplicationTask (() => {
				this.DeleteItem (id);
			});
		}

		public void EditingDidStart ()
		{
		}

		public void EditingDidEnd ()
		{
			int i = 0;
			foreach (SubtitleCellModel currentCell in _data) {
				if (currentCell.Selected) {
					_selectedRowIndex = i;
					this.SendEvent (this.HighlightSelectedRowEvent);
					break;
				}
				i++;
			}
		}

		public SubtitleCellModel SubtitleCellViewModel (NSIndexPath indexPath)
		{
			return this.CellViewModel (indexPath);
		}

		protected override void SelectRow (NSIndexPath indexPath)
		{
			SubtitleCellModel cellModel = _data [indexPath.Row];
			int id = cellModel.ID;
			this.AddApplicationTask (() => {
				this.SelectItem (id);
			});
		}

		protected void SendSelectRowEvent (int selectedCellID)
		{
			this.AddGUITask (() => {
				SubtitleCellModel[] data = _data.ToArray ();
				int index = 0;
				foreach (SubtitleCellModel item in data) {
					item.Selected = false;
					if (item.ID == selectedCellID) {
						item.Selected = true;
						_selectedRowIndex = index;
					}
					index++;
				}
				this.SendEvent (this.SelectRowEvent);
			});
		}

		protected void SendReloadRowEvent ()
		{
			this.SendEvent (this.ReloadRowEvent);
		}

		protected void SendMoveRowEvent ()
		{
			this.SendEvent (this.MoveRowEvent);
		}
	}
}

