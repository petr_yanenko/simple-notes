﻿using System;

namespace Notes
{
	public interface IViewModel
	{
		bool Loading { get; }
		event PropertyDidChangeHandler LoadingEvent;

		event PropertyDidChangeHandler NewDataEvent;

		string ErrorTitle { get; }
		string ErrorMessage { get; }
		event PropertyDidChangeHandler ErrorEvent;

		void ReloadAction ();
		void LoadDataAction ();
		void CancelAction ();
	}
}

