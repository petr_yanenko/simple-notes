﻿using System;
using System.Collections.Generic;

using Foundation;

namespace Notes
{
	public class NotesViewModel : EditableTableViewModel<NotesCellModel>
	{
		public NotesViewModel (IModel model) : base (model)
		{
		}

		public void CreateNewNote ()
		{
			this.AddApplicationTask (() => {
				this.Model.InsertNote ();
			});
		}

		protected override void Reload ()
		{
			this.Model.SelectedFolder.ResetNotes ();
			this.Model.LoadNotes ();
		}

		protected override void LoadData ()
		{
			this.Model.LoadNotes ();
		}

		protected override void DeleteItem (int id)
		{
			this.Model.DeleteNote (id);
		}

		protected override int RowsCount (int batch)
		{
			return this.Model.NotesCount;
		}

		protected override void SelectItem (int id)
		{
			this.Model.SelectNote (id);
		}

		protected override void AddObservers (IModel model)
		{
			base.AddObservers (model);
			this.Model.SelectNoteEvent += (object sender) => {
				int selectedNoteID = this.Model.SelectedNote.ID;
				this.SendSelectRowEvent (selectedNoteID);
			};
			this.Model.SelectedNoteDidChangeEvent += (object sender) => {
				DBNote selectedNote = this.Model.SelectedNote;
				int selectedNoteID = selectedNote.ID;
				string firstPart = selectedNote.Content.Count > 0 ? selectedNote.Content [0] : null;
				DateTime date = selectedNote.LastEdit;
				this.AddGUITask (() => {
					int index;
					for (index = 0; index < _data.Count; index++) {
						NotesCellModel cellModel = _data [index];
						if (selectedNoteID == cellModel.ID) {
							cellModel.Configure (firstPart, date);
							break;
						}
					}
					if (index > 0) {
						NotesCellModel movedCell = _data [index];
						_data.RemoveAt (index);
						_data.Insert (0, movedCell);
						_fromIndex = index;
						this.SendMoveRowEvent ();
					}
					_reloadIndex = 0;
					this.SendReloadRowEvent ();
				});
			};
		}

		protected override List<NotesCellModel> CreateViewData ()
		{
			List<NotesCellModel> data = base.CreateViewData ();
			data.Sort ((NotesCellModel first, NotesCellModel second) => {
				return first.LastEdited.CompareTo (second.LastEdited) * -1;
			});
			return data;
		}
	}
}

