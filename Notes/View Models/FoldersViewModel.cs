﻿using System;

using UIKit;
using Foundation;

namespace Notes
{
	public class FoldersViewModel : EditableTableViewModel<FoldersCellModel>
	{
		bool _showInsertDialog = false;
		public bool ShowInsertDialog { get { return _showInsertDialog; } }
		public event PropertyDidChangeHandler ShowInsertDialogEvent;

		public FoldersViewModel (IModel model) : base (model)
		{
		}

		public void InsertFolderAction ()
		{
			this.SetShowInsertDialog (true);
		}

		public void SaveInsertedFolderAction (string folder)
		{
			this.SetShowInsertDialog (false);
			if (!string.IsNullOrWhiteSpace (folder)) {
				string name = folder;
				this.AddApplicationTask (() => {
					this.Model.InsertFolder (name);
				});
			}
		}
		public void CancelInsertFolderAction ()
		{
			this.SetShowInsertDialog (false);
		}

		protected override void DeleteItem (int id)
		{
			this.Model.DeleteFolder (id);
		}

		protected override int RowsCount (int batch)
		{
			return this.Model.Count;
		}

		protected override void SelectItem (int id)
		{
			this.Model.SelectItem (id);
		}

		protected override void AddObservers (IModel model)
		{
			base.AddObservers (model);
			this.Model.SelectItemEvent += (object sender) => {
				int selectedFolderID = this.Model.SelectedFolder.ID;
				this.SendSelectRowEvent (selectedFolderID);
			};
		}

		void SetShowInsertDialog (bool show)
		{
			_showInsertDialog = show;
			this.SendEvent (this.ShowInsertDialogEvent);
		}
	}
}

