﻿using System;
using System.Collections.Generic;
using System.IO;

using Foundation;
using MobileCoreServices;
using UIKit;
using CoreGraphics;
using AVFoundation;
using CoreMedia;

namespace Notes
{
	public class ContentViewModel : IOSViewModel<List<NSMutableAttributedString>>
	{
		const string kIndexKey = "index";
		const string kTypeKey = "type";
		const string kAttachmentKey = "attachment";
		const string kAspectRatioKey = "ratio";
		const string kAttachmentPathKey = "path";

		const int kAttachmentMargin = 10;

		const float kContentFont = 20f;

		DBNote _selectedNote = null;

		NSCache _attachmentCache = new NSCache ();

		public CGRect SplitViewContainerBounds { get; set; }
		public CGRect TextViewBounds { get; set; }
		public NSRange SelectedRange { get; set; }
		public bool IsPhone { get; set; }
		int _changedLength;
		public int ChangedLength { get { return _changedLength; } }

		bool _enabled = false;
		public bool Enabled { 
			get { return _enabled; } 
			private set {
				_enabled = value;
				this.SendEvent (this.EnabledEvent);
			}
		}
		public event PropertyDidChangeHandler EnabledEvent;

		bool _showAttachmentDialog = false;
		public bool ShowAttachmentDialog { get { return _showAttachmentDialog; } }
		public event PropertyDidChangeHandler ShowAttachmentDialogEvent;

		UIImagePickerControllerSourceType _pickerType;
		public UIImagePickerControllerSourceType PickerType { get { return _pickerType; } }

		bool _showImagePiker = false;
		public bool ShowImagePiker { get { return _showImagePiker; } }
		public event PropertyDidChangeHandler ShowImagePikerEvent;

		public override string NavigationTitle {
			get { return ""; }
		}

		string _attachmentPath = null;
		public string AttachmentPath { get { return _attachmentPath; } }
		public event PropertyDidChangeHandler ShowOriginImageEvent;
		public event PropertyDidChangeHandler ShowOriginVideoEvent;

		bool _rotation = false;

		NotesModel Model { get { return (NotesModel)_model; } }

		public ContentViewModel (IModel model) : base (model)
		{
			this.AddApplicationTask (() => {
				this.SetData ();
			});
		}

		public NSMutableAttributedString Content ()
		{
			NSMutableAttributedString content = new NSMutableAttributedString ();
			if (this.IsViewDataExist ()) {
				_data.FindAll (item => {
					NSRange attributesRange;
					NSMutableDictionary attributes = (NSMutableDictionary) item.GetAttributes (0, out attributesRange).MutableCopy ();
					NSData attachment = (NSData) attributes [kAttachmentKey];
					if (attachment != null) {
						NSTextAttachment textAttachment = new NSTextAttachment (
							attachment, 
							UTType.Image
						);
						if (!this.IsPhone) {
							nfloat attachmentWidth = this.GetAttachmentWidth (this.TextViewBounds.Width);
							NSNumber aspectRatio = (NSNumber) attributes [kAspectRatioKey];
							nfloat attachmentHeight = this.GetAttachmentHeight (attachmentWidth, aspectRatio.FloatValue);
							textAttachment.Bounds = new CGRect (0, 0, attachmentWidth, attachmentHeight);
						}
						NSMutableAttributedString attachmentString = new NSMutableAttributedString (
							NSAttributedString.CreateFrom (textAttachment)
						);
						NSRange attachmentRange = new NSRange (0, attachmentString.Length);
						attributes.Remove (new NSString (kAttachmentKey));
						attachmentString.AddAttributes (attributes, attachmentRange);
						attachmentString = this.AddAlignment (attachmentString, UITextAlignment.Center, attachmentRange);
						item.SetString (attachmentString);
					} else {
						NSRange effectiveRange;
						NSParagraphStyle style = (NSParagraphStyle) item.GetAttribute (UIStringAttributeKey.ParagraphStyle, 0, out effectiveRange);
						if (style == null) {
							item = this.AddAlignment (item, UITextAlignment.Left, new NSRange (0, item.Length));
						}
					}
					content.Append (item);
					return true;
				});
			}
			NSRange contentRange = new NSRange (0, content.Length);
			content.AddAttribute (
				UIStringAttributeKey.Font, 
				UIFont.SystemFontOfSize (kContentFont), 
				contentRange
			);
			return content;         
		}

		public void CreateNewNote ()
		{
			this.AddApplicationTask (() => {
				this.Model.InsertNote ();
			});
		}

		public bool ShouldChangeContent (NSAttributedString content, NSRange range, string text)
		{
			content = this.Content ();
			if (this.IsViewDataExist ()) {
				_changedLength = text.Length;
				if (_changedLength == 0 && this.SelectedRange.Location > range.Location) {
					_changedLength = - (int)range.Length;
				}
				if (this.isDataNotEmpty ()) {
					NSRange attributesRange;
					int index = this.GetNotePartIndex (content, range, out attributesRange);
					this.ChangeNotePart (index, range.Location - attributesRange.Location, range.Length, text);
				} else if (text.Length > 0) {
					this.InsertNotePart (0, 0, 0, text, DBNote.kTextCode);
				}
			}
			return false;
		}

		public void AddAttachmentAction ()
		{
			if (this.IsViewDataExist ()) {
				this.SetShowAttachmentDialog (true);
			}
		}

		public void PhotoLibraryAction ()
		{
			_pickerType = UIImagePickerControllerSourceType.PhotoLibrary;
			this.SetShowPickerController (true);
			this.SetShowAttachmentDialog (false);
		}

		public void CameraAction ()
		{
			_pickerType = UIImagePickerControllerSourceType.Camera;
			this.SetShowPickerController (true);
			this.SetShowAttachmentDialog (false);
		}

		public void CancelPickerDialogAction ()
		{
			this.SetShowAttachmentDialog (false);
		}

		public void InsertImageAttachmentAction (UIImage image, NSAttributedString content, NSRange selectedRange)
		{
			content = this.Content ();
			CGRect bounds = this.SplitViewContainerBounds;
			this.InsertAttachment (content, selectedRange, DBNote.kImageCode, () => {
				float aspectRatio = this.GetAspectRatio (image);
				UIImage resizedImage = this.ResizeImage (image, bounds);
				string attachmentPath = this.SaveAttachment (resizedImage, Constants.OriginImageExtension, "image", (string originPath) => {
					File.WriteAllBytes (originPath, image.AsJPEG ().ToArray ());
				});
				return new Tuple<float, string> (aspectRatio, attachmentPath);
			});
		}

		public void InsertMovieAttachmentAction (string url, NSAttributedString content, NSRange selectedRange)
		{
			content = this.Content ();
			Random random = new Random ();
			string tempPath = Path.Combine (Constants.TempDirectory, random.Next ().ToString ());
			tempPath = Path.ChangeExtension (tempPath, Path.GetExtension (url));
			File.Move (url, tempPath);
			CGRect bounds = this.SplitViewContainerBounds;
			nfloat width = this.GetAttachmentWidth (this.GetMinDimension (bounds));
			this.CreateThumbnailImageFromURL (tempPath, width, width * 2, image => {
				if (image != null) {
					this.InsertAttachment (content, selectedRange, DBNote.kVideoCode, () => {
						float aspectRatio = this.GetAspectRatio (image);
						UIImage resizedImage = this.ResizeImage (image, bounds);
						string attachmentPath = this.SaveAttachment (resizedImage, Constants.OriginMovieExtension, "movie", (string originPath) => {
							File.Move (tempPath, originPath);
						});

						return new Tuple<float, string> (aspectRatio, attachmentPath);
					});
				}
			});
		}

		public void DismissImagePickerAction ()
		{
			this.SetShowPickerController (false);
		}

		public void CancelImagePickerShowingAction ()
		{
			_showImagePiker = false;
		}

		public void RotateAction ()
		{
			this.AddApplicationTask (() => {
				_rotation = true;
				this.SetData ();
				_rotation = false;
			});
		}

		public void AttachmentTapAction (NSAttributedString text, int characterIndex)
		{
			NSRange effectiveRange;
			NSDictionary attributes = text.GetAttributes (characterIndex, out effectiveRange);
			string type = (NSString)attributes [kTypeKey];
			string path = (NSString)attributes [kAttachmentPathKey];
			PropertyDidChangeHandler showOriginContentEvent = null;
			if (type == DBNote.kImageCode) {
				path = Path.ChangeExtension (path, Constants.OriginImageExtension);
				showOriginContentEvent = this.ShowOriginImageEvent;
			} else if (type == DBNote.kVideoCode) {
				path = Path.ChangeExtension (path, Constants.OriginMovieExtension);
				showOriginContentEvent = this.ShowOriginVideoEvent;
			}
			_attachmentPath = path;
			this.SendEvent (showOriginContentEvent);
			_attachmentPath = null;
		}

		NSMutableAttributedString AddAlignment (NSMutableAttributedString oldString, UITextAlignment alignment, NSRange range)
		{
			NSMutableParagraphStyle attachmentStyle = new NSMutableParagraphStyle ();
			attachmentStyle.Alignment = alignment;
			oldString.AddAttribute (UIStringAttributeKey.ParagraphStyle, attachmentStyle, range);
			return oldString;
		}

		UIImage ResizeImage (UIImage originImage, CGRect bounds)
		{
			float aspectRatio = this.GetAspectRatio (originImage);
			nfloat width = this.GetAttachmentWidth (this.GetMinDimension (bounds));
			CGSize size = new CGSize (width, this.GetAttachmentHeight (width, aspectRatio));
			UIGraphics.BeginImageContext (size);
			originImage.Draw (new CGRect (0, 0, size.Width, size.Height));
			UIImage resizedImage = UIGraphics.GetImageFromCurrentImageContext ();
			UIGraphics.EndImageContext ();
			return resizedImage;
		}

		UIImage DrawText (string text, UIImage image)
		{
			float fontSize = 36;
			UIFont font = UIFont.BoldSystemFontOfSize (fontSize);
			UIGraphics.BeginImageContext (image.Size);
			CGRect rect = new CGRect (0, 0, image.Size.Width, image.Size.Height);
			image.Draw (rect);
			UIColor.White.SetColor ();
			CGRect textRect = new CGRect (0, (rect.Height - fontSize) / 2, rect.Width, rect.Height / 2);
			text.DrawString (textRect, font, UILineBreakMode.WordWrap, UITextAlignment.Center); 
			UIImage newImage = UIGraphics.GetImageFromCurrentImageContext();
			UIGraphics.EndImageContext();

			return newImage;
		}

		string SaveAttachment (UIImage preview, string originExtension, string previewText, Action<string> saveHandler)
		{
			string attachmentPath = this.Model.FullFilePath (_selectedNote.ID.ToString ());
			string previewPath = Path.ChangeExtension (attachmentPath, Constants.PreviewImageExtension);
			string originPath = Path.ChangeExtension (attachmentPath, originExtension);
			UIImage image = this.DrawText (
				NSBundle.MainBundle.LocalizedString ("Tap to view " + previewText, null),
				preview
			);
			byte[] imageBytes = image.AsJPEG ().ToArray ();
			File.WriteAllBytes (previewPath, imageBytes);
			saveHandler (originPath);
			return attachmentPath;
		}

		void CreateThumbnailImageFromURL (string videoPath, nfloat thumbWidth, nfloat thumbHeight, Action<UIImage> completion)
		{
			NSUrl videoURL = NSUrl.FromFilename (videoPath);
			AVUrlAsset asset = new AVUrlAsset (videoURL);
			AVAssetImageGenerator generator = new AVAssetImageGenerator (asset);
			generator.AppliesPreferredTrackTransform = true;

			CMTime thumbTime = new CMTime (1, 60);
			AVAssetImageGeneratorCompletionHandler handler = (CMTime requestedTime, IntPtr imageRef, CMTime actualTime, AVAssetImageGeneratorResult result, NSError error) => {
				UIImage thumbImg = null;
				if (result != AVAssetImageGeneratorResult.Succeeded) {
					Console.WriteLine ("couldn't generate thumbnail, error:{0}", error.ToString ());
				} else {
					CGImage image = new CGImage (imageRef);
					thumbImg = new UIImage (image);
				}
				this.AddGUITask (() => {
					completion (thumbImg);
				});
			};

			CGSize maxSize = new CGSize (thumbWidth, thumbHeight);
			generator.MaximumSize = maxSize;
			generator.GenerateCGImagesAsynchronously (new NSValue[] {NSValue.FromCMTime (thumbTime) }, handler);
		}

		nfloat GetAttachmentWidth (nfloat width)
		{
			return width - kAttachmentMargin * 2;
		}

		nfloat GetMinDimension (CGRect bounds)
		{
			return Math.Min ((int)bounds.Width, (int)bounds.Height);
		}

		nfloat GetAttachmentHeight (nfloat width, float aspectRatio)
		{
			return width * aspectRatio;
		}

		float GetAspectRatio (UIImage image)
		{
			return (float) (image.Size.Height / image.Size.Width);
		}
			
		void InsertNotePart (int index, int location, int length, string content, string type)
		{
			this.AddApplicationTask (() => {
				this.Model.InsertNotePart (index, location, length, content, type);
			});
		}

		void ChangeNotePart (int index, nint location, nint length, string text)
		{
			this.AddApplicationTask (() => {
				this.Model.ChangeNotePart (index, location, length, text);
			});
		}

		int GetNotePartIndex (NSAttributedString content, NSRange range, out NSRange attributesRange)
		{
			nint location = range.Location;

			if (range.Location == content.Length) {
				location -= 1;
			}
				
			NSDictionary attributes = content.GetAttributes (location, out attributesRange);
			NSNumber number = (NSNumber)attributes [kIndexKey];
			int index = number.Int32Value;
			return index;
		}

		void InsertAttachment (
			NSAttributedString content, 
			NSRange selectedRange, 
			string type,
			Func<Tuple<float, string>> prepareContent
		)
		{
			if (this.IsViewDataExist ()) {
				_changedLength = Constants.AttachmentLength;
				NSRange attributesRange;
				int index = 0, location = 0, length = 0;
				if (this.isDataNotEmpty ()) {
					index = this.GetNotePartIndex (content, selectedRange, out attributesRange);
					location = (int)(selectedRange.Location - attributesRange.Location);
					length = (int)selectedRange.Length;
				}
				this.AddApplicationTask (() => {
					Tuple<float, string> result = prepareContent ();
					string notePart = 
						result.Item1.ToString () +
						Constants.AttachmentDivider +
						Path.Combine (_selectedNote.ID.ToString (), Path.GetFileName (result.Item2));
					this.InsertNotePart (
						index, location, length, notePart, type
					);
				});
				this.SetShowPickerController (false);
			}
		}

		protected override void AddObservers (IModel model)
		{
			PropertyDidChangeHandler newContent = (object sender) => {
				this.SetData ();
			};
			this.Model.SelectItemEvent += newContent;
			this.Model.SelectNoteEvent += newContent;
			this.Model.NewDataEvent += newContent;
			this.Model.SelectedNoteDidChangeEvent += newContent;
		}

		void SetData ()
		{
			DBNote note = this.Model.SelectedNote;
			bool reset = _selectedNote == null || note == null || _selectedNote.ID != note.ID;
			_selectedNote = note;
			List<NSMutableAttributedString> data = this.CreateViewData ();
			this.SetData (data, reset);
		}

		void SetData (List<NSMutableAttributedString> data, bool reset)
		{
			bool rotation = _rotation;
			this.AddGUITask (() => {
				if (rotation || reset) {
					_changedLength = 0;
				}
				if (reset) {
					this.SelectedRange = new NSRange (0, 0);
					this.SetShowAttachmentDialog (false);
					this.SetShowPickerController (false);
				}
				this.GUITaskSetData (data);
			});
		}

		protected override void DataDidSet ()
		{
			base.DataDidSet ();
			this.Enabled = this.IsViewDataExist ();
		}

		protected override List<NSMutableAttributedString> CreateViewData ()
		{
			if (_selectedNote == null) {
				return null;
			}
			int index = 0;
			return _selectedNote.Content.ConvertAll<NSMutableAttributedString> (item => {
				string content = item.Substring (DBNote.kTypeLength);
				string type = item.Substring (0, DBNote.kTypeCodeLength);
				NSDictionary attributes = NSDictionary.FromObjectsAndKeys (
					new object[] {new NSNumber (index), type},
					new string[] {kIndexKey, kTypeKey}
				);
				NSMutableAttributedString contentPart;
				if (type == DBNote.kTextCode) {
					contentPart = new NSMutableAttributedString (content, attributes);
				} else {
					float aspectRatio;
					string path = this.Model.GetMediaPath (content, out aspectRatio);
					NSData file = (NSData)_attachmentCache.ObjectForKey (new NSString (path));
					string filePath = Path.Combine (Constants.ResourcesDirectory, path);
					if (file == null) {
						filePath = Path.ChangeExtension (filePath, Constants.PreviewImageExtension);
						file = NSData.FromFile (filePath);
						_attachmentCache.SetObjectforKey (file, new NSString (content));
					}
					NSMutableAttributedString attachmentString = new NSMutableAttributedString (" ");
					NSRange range = new NSRange (0, attachmentString.Length);
					attachmentString.AddAttribute (new NSString (kIndexKey), new NSNumber (index), range);
					attachmentString.AddAttribute (new NSString (kTypeKey), new NSString (type), range);
					attachmentString.AddAttribute (new NSString (kAttachmentKey), file, range);
					attachmentString.AddAttribute (new NSString (kAspectRatioKey), new NSNumber (aspectRatio), range);
					attachmentString.AddAttribute (new NSString (kAttachmentPathKey), new NSString (filePath), range);
					contentPart = attachmentString;
				}
				index++;
				return contentPart;
			});
		}

		void SetShowAttachmentDialog (bool show)
		{
			_showAttachmentDialog = show;
			this.SendEvent (this.ShowAttachmentDialogEvent);
		}

		void SetShowPickerController (bool show)
		{
			_showImagePiker = show;
			this.SendEvent (this.ShowImagePikerEvent);
		}

		bool IsViewDataExist ()
		{
			return _data != null;
		}

		bool isDataNotEmpty ()
		{
			return this.IsViewDataExist () && _data.Count > 0;
		}
	}
}

