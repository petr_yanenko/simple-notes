﻿using System;

using Foundation;

namespace Notes
{
	public interface IEditableTableViewModel : ITableViewModel
	{
		NSIndexPath DeletedPath { get; }
		event PropertyDidChangeHandler DeletedPathEvent;

		NSIndexPath SelectedRowPath { get; }
		event PropertyDidChangeHandler SelectRowEvent;
		event PropertyDidChangeHandler HighlightSelectedRowEvent;

		NSIndexPath FromIndexPath { get; }
		NSIndexPath ToIndexPath { get; }
		event PropertyDidChangeHandler MoveRowEvent;

		NSIndexPath ReloadIndexPath { get; }
		event PropertyDidChangeHandler ReloadRowEvent;

		void DeleteRowAction (NSIndexPath indexPath);
		SubtitleCellModel SubtitleCellViewModel (NSIndexPath indexPath);

		void EditingDidStart ();
		void EditingDidEnd ();
	}
}

