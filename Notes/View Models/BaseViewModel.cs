﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Notes
{
	public abstract class BaseViewModel<T> : IViewModel
	{
		protected IModel _model;

		protected T _data = default (T);
		public T Data { get { return _data; } }

		string _errorTitle = null;
		string _errorMessage = null;
		public string ErrorTitle { get { return _errorTitle; } }
		public string ErrorMessage { get { return _errorMessage; } }

		bool _loading = false;
		public bool Loading { get { return _loading; } }

		public event PropertyDidChangeHandler LoadingEvent;
		public event PropertyDidChangeHandler NewDataEvent;
		public event PropertyDidChangeHandler ErrorEvent;

		protected abstract void DisableGUITasks ();
		protected abstract void AddGUITask (Action action);
		protected abstract void AddApplicationTask (Action Action);
		protected abstract T CreateViewData ();

		public BaseViewModel (IModel model)	
		{
			_model = model;
			if (_model != null) {
				this.AddObservers (model);
			}
		}

		public virtual void ReloadAction ()
		{
			this.AddApplicationTask (() => {
				this.Reload ();
			});
		}

		public virtual void LoadDataAction ()
		{
			this.AddApplicationTask (() => {
				this.LoadData ();
			});
		}

		public virtual void CancelAction ()
		{
			this.DisableGUITasks ();
			_model.RemoveObserver (this);
			this.AddApplicationTask (() => {
				this.Cancel ();
			});
		}

		protected virtual void Reload ()
		{
			_model.Reset ();
			_model.LoadData ();
		}

		protected virtual void LoadData ()
		{
			_model.LoadData ();
		}

		protected virtual void Cancel ()
		{
			_model.Cancel ();
		}

		protected virtual void AddObservers (IModel model)
		{
			_model.LoadingEvent += sender => { 
				bool loading = _model.Loading;
				this.AddGUITask (() => {
					_loading = loading;
					this.SendEvent (this.LoadingEvent);
				});
			};
			_model.ErrorEvent += sender => {
				string errorReason = _model.ErrorReason;
				this.AddGUITask (() => {
					this.SendError (errorReason);
				});
			};
			_model.NewDataEvent += sender => {
				T data = this.CreateViewData ();
				this.SetData (data);
			};
		}

		protected virtual void SetData (T data)
		{
			this.AddGUITask (() => {
				this.GUITaskSetData (data);
			});
		}

		protected virtual void GUITaskSetData (T data)
		{
			_data = data;
			this.SendEvent (this.NewDataEvent);
			this.DataDidSet ();
		}

		protected virtual void DataDidSet ()
		{
			
		}

		protected virtual void CreateError (string errorReason, out string title, out string message)
		{
			title = null;
			message = null;
		}

		protected void SendEvent (PropertyDidChangeHandler newEvent)
		{
			if (newEvent != null) newEvent (this);
		}

		void SendError (string errorReason)
		{
			string title;
			string message;
			this.CreateError (errorReason, out title, out message);
			this.SendError (title, message);
		}

		void SendError (string title, string message)
		{
			_errorTitle = title;
			_errorMessage = message;
			this.SendEvent (this.ErrorEvent);
			_errorTitle = null;
			_errorMessage = null;
		}
	}
}

