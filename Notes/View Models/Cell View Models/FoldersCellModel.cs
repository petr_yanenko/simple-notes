﻿using System;

using Foundation;

namespace Notes
{
	public class FoldersCellModel : SubtitleCellModel
	{
		string _title = null;
		public override string Title { get { return _title; } }

		int _count = 0;
		public override string Subtitle { 
			get { 
				return _count.ToString () + " " + NSBundle.MainBundle.LocalizedString ("notes", null); 
			} 
		}

		public FoldersCellModel () : base ()
		{
		}

		public override void Configure (NSIndexPath indexPath)
		{
			base.Configure (indexPath);
			DBFolder folder = this.GetFolder (indexPath);
			_title = folder.Title;
			_count = folder.Count;
		}

		protected override DBObject GetDataObject (NSIndexPath indexPath)
		{
			return this.GetFolder (indexPath);
		}

		DBFolder GetFolder (NSIndexPath indexPath)
		{
			return this.Model.GetItem (indexPath.Row);
		}
	}
}

