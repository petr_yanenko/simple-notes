﻿using System;

using Foundation;

namespace Notes
{
	public abstract class IOSCellViewModel : BaseCellViewModel
	{
		public IOSCellViewModel () : base () {}

		public abstract void Configure (NSIndexPath indexPath);
	}
}

