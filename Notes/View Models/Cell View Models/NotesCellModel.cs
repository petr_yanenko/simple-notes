﻿using System;

using Foundation;

namespace Notes
{
	public class NotesCellModel : SubtitleCellModel
	{
		string _firstFragment = null;
		public override string Title { get { return _firstFragment; } }

		DateTime _lastEdited = default(DateTime);
		public DateTime LastEdited { get { return _lastEdited; } }
		public override string Subtitle { 
			get { 
				return string.Format (
					NSBundle.MainBundle.LocalizedString ("Last edited at", null) + " {0}", 
					_lastEdited.ToString (Constants.DateFormat)
				);
			} 
		}

		public NotesCellModel () : base ()
		{
		}

		public override void Configure (NSIndexPath indexPath)
		{
			base.Configure (indexPath);
			DBNote note = this.GetNote (indexPath);

			this.Configure (note.Content.Count > 0 ? note.Content [0] : null, note.LastEdit);
		}

		public void Configure (string firstPart, DateTime date)
		{
			string type = null, fragment = null;
			if (firstPart != null) {
				fragment = firstPart;
				type = fragment.Substring (0, DBNote.kTypeCodeLength);
			}
			if (type == DBNote.kTextCode) {
				_firstFragment = fragment.Substring (DBNote.kTypeLength);
			} else if (type == DBNote.kImageCode) {
				_firstFragment = NSBundle.MainBundle.LocalizedString ("Photo", null);
			} else if (type == DBNote.kVideoCode) {
				_firstFragment = NSBundle.MainBundle.LocalizedString ("Video", null);
			} else {
				_firstFragment = "";
			}

			_lastEdited = date;
		}

		protected override DBObject GetDataObject (NSIndexPath indexPath)
		{
			return this.GetNote (indexPath);
		}

		DBNote GetNote (NSIndexPath indexPath)
		{
			return this.Model.GetNote (indexPath.Row);
		}
	}
}

