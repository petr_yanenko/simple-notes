﻿using System;

using Foundation;

namespace Notes
{
	public abstract class SubtitleCellModel : IOSCellViewModel
	{
		protected int _id = 0;
		public int ID { get { return _id; } }

		public virtual string Title { get; }

		public virtual string Subtitle { get; }

		public bool Selected { get; set; }

		protected abstract DBObject GetDataObject (NSIndexPath indexPath);

		protected NotesModel Model { get { return (NotesModel)_model; } }

		public SubtitleCellModel () : base ()
		{
		}

		public override void Configure (NSIndexPath indexPath)
		{
			this.Configure (this.GetDataObject (indexPath));
		}

		void Configure (DBObject data)
		{
			_id = data.ID;
			this.Selected = data.Selected;
		}
	}
}

