﻿using System;

namespace Notes
{
	public class BaseCellViewModel
	{
		protected IViewModel _viewModel = null;
		protected IModel _model = null;

		public BaseCellViewModel () {}

		public virtual void Initialize (IViewModel viewModel, IModel model)
		{
			_viewModel = viewModel;
			_model = model;
		}
	}
}

