﻿using System;
using System.Collections.Generic;

using Foundation;

namespace Notes
{
	public abstract class TableViewModel<T> : IOSViewModel<List<T>>, ITableViewModel where T : IOSCellViewModel, new ()
	{
		public TableViewModel (IModel model) : base (model) {}

		public abstract string CellReuseIdentifier (NSIndexPath indexPath);
		public abstract nfloat RowHeight (nfloat tableHeight, NSIndexPath indexPath);
		protected abstract int RowsCount (int batch);

		public virtual void SelectRowAction (NSIndexPath indexPath)
		{
			this.AddApplicationTask (() => {
				this.SelectRow (indexPath);
			});
		}

		public virtual T CellViewModel (NSIndexPath indexPath)
		{
			return _data[indexPath.Row];
		}

		public virtual int SectionsNumber ()
		{
			return this.CellsNumber (0) > 0 ? 1 : 0;
		}

		public virtual int CellsNumber (nint section)
		{
			return _data != null ? _data.Count : 0;
		}

		public nfloat RowHeight ()
		{
			return this.RowHeight (0);
		}

		public nfloat RowHeight (nfloat tableHeight)
		{
			return this.RowHeight (tableHeight, null);
		}

		protected virtual void SelectRow (NSIndexPath indexPath) {}

		protected virtual int BatchesCount ()
		{
			return 1;
		}

		protected override List<T> CreateViewData ()
		{
			List<T> data = new List<T> ();
			for (int i = 0; i < this.RowsCount (0); i++) {
				T item = this.InitializeItem (new T ());
				item.Configure (NSIndexPath.FromRowSection (i, 0));
				data.Add (item);
			}
			return data;
		}

		protected virtual T InitializeItem (T item)
		{
			item.Initialize (this, _model);
			return item;
		}
	}
}

