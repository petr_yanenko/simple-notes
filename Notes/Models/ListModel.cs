﻿using System;

namespace Notes
{
	public abstract class ListModel<T, U> : BaseModel<T>, IListModel<U>
	{
		public abstract int Count { get; }
		public abstract void SelectItem (int index);
		public abstract U GetItem (int index);
	}
}

