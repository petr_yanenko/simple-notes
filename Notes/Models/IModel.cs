﻿using System;

namespace Notes
{
	public interface IModel
	{
		bool Loading { get; }
		event PropertyDidChangeHandler LoadingEvent;

		event PropertyDidChangeHandler NewDataEvent;

		string ErrorReason { get; }
		event PropertyDidChangeHandler ErrorEvent;

		void Reset ();
		void LoadData ();
		void Cancel ();
		void RemoveObserver (object observer);
	}
}

