﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Notes
{
	public abstract class BaseModel<T> : IModel
	{
		protected delegate void ConnectCompletion ();

		protected bool _loading = false;
		public bool Loading { get { return _loading; } }
		public event PropertyDidChangeHandler LoadingEvent;

		protected T _data = default (T);
		public event PropertyDidChangeHandler NewDataEvent;

		string _errorReason = null;
		public string ErrorReason { get { return _errorReason; } }
		public event PropertyDidChangeHandler ErrorEvent;

		public abstract void Reset ();

		public abstract void LoadData ();

		public abstract void Cancel ();

		public virtual void RemoveObserver (object observer)
		{
			this.RemoveObserver (observer, ref this.NewDataEvent);
			this.RemoveObserver (observer, ref this.LoadingEvent);
			this.RemoveObserver (observer, ref this.ErrorEvent);
		}

		protected virtual void RemoveObserver (object observer, ref PropertyDidChangeHandler eventObject)
		{
			Delegate[] handlers = eventObject.GetInvocationList ();
			Delegate[] removedHandlers = Array.FindAll<Delegate> (handlers, (Delegate item) => {
				if (item.Target.Equals (observer)) {
					return true;
				}
				return false;
			});
			foreach (PropertyDidChangeHandler item in removedHandlers) {
				eventObject -= item;
			}
		}

		protected void SetErrorReason (string errorReason)
		{
			_errorReason = errorReason;
			this.SendEvent (this.ErrorEvent);
		}

		protected void SetLoading (bool loading)
		{
			if (_loading != loading) {
				_loading = loading;
				this.SendEvent (this.LoadingEvent);
			}
		}

		protected void SendNewDataEvent ()
		{
			this.SendEvent (this.NewDataEvent);
		}

		protected void SendEvent (PropertyDidChangeHandler newEvent)
		{
			if (newEvent != null) newEvent (this);
		}
	}
}

