﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace Notes
{
	public class NotesModel : ListModel<List<DBFolder>, DBFolder>
	{
		public override int Count { get { return _data != null ? _data.Count : 0; } }
		public int NotesCount { 
			get { 
				DBFolder folder = this.SelectedFolder;
				if (folder != null) {
					return folder.Notes.Count; 
				}
				return 0;
			} 
		}

		public DBFolder SelectedFolder { 
			get {
				return this.SelectedObject<DBFolder> (_data.ToArray ());
			}
		}

		public DBNote SelectedNote {
			get {
				DBFolder folder = this.SelectedFolder;
				if (folder != null) {
					return this.SelectedObject<DBNote> (folder.Notes.ToArray ());
				}
				return null;
			}
		}

		public event PropertyDidChangeHandler SelectItemEvent;
		public event PropertyDidChangeHandler SelectNoteEvent;
		public event PropertyDidChangeHandler SelectedNoteDidChangeEvent;

		static NotesModel _sharedInstance = new NotesModel ();

		public static NotesModel SharedInstance ()
		{
			return _sharedInstance;
		}

		NotesModel () : base ()
		{
			_data = new List<DBFolder> ();
		}

		public override void Reset ()
		{
			if (_data.Count > 0) {
				_data.RemoveAll (folder => {
					return true;
				});
			}
		}

		public override void LoadData ()
		{
			if (this.Load ()) {
				this.SendNewDataEvent ();
			}
		}

		public void LoadNotes ()
		{
			this.SendNewDataEvent ();
		}

		public override void RemoveObserver (object observer)
		{
			base.RemoveObserver (observer);
			this.RemoveObserver (observer, ref this.SelectItemEvent);
		}

		public override void Cancel ()
		{
			
		}

		public override void SelectItem (int id)
		{
			DBFolder selectedFolder = this.FindObject<DBFolder> (id, _data);
			if (selectedFolder != null) {
				this.UnselectFolders (null);
				selectedFolder.Selected = true;

				List<DBNote> notes = selectedFolder.Notes;
				bool hasSelectedNote = notes.Any<DBNote> (note => {
					if (note.Selected) {
						return true;
					}
					return false;
				});
				if (!hasSelectedNote && notes.Count > 0) {
					DBNote lastNote = null;
					foreach (DBNote currentNote in notes) {
						if (lastNote == null || lastNote.LastEdit.CompareTo (currentNote.LastEdit) < 0) {
							lastNote = currentNote;
						}
					}
					lastNote.Selected = true;
				}
				this.SendEvent (this.SelectItemEvent);
				this.SaveAll ();
			}
		}

		public void SelectNote (int id)
		{
			DBFolder selectedFolder = this.SelectedFolder;
			DBNote selectedNote = this.FindNote (id);
			if (selectedNote != null) {
				this.UnselectNotes (selectedFolder);
				selectedNote.Selected = true;
				this.SendEvent (this.SelectNoteEvent);
				this.SaveAll ();
			}
		}

		public string FullFilePath (string noteID)
		{
			DirectoryInfo directory = Directory.CreateDirectory (
				Path.Combine (Constants.ResourcesDirectory, noteID)
			);
			IEnumerable<FileInfo> files = directory.EnumerateFiles ();
			int lastFileNumber = 0;
			foreach (FileInfo currentFile in files) {
				int currentNumber = int.Parse (Path.GetFileNameWithoutExtension (currentFile.Name));
				if (currentNumber > lastFileNumber) {
					lastFileNumber = currentNumber;
				}
			}
			string fileName = string.Format ("{0}", ++lastFileNumber);
			return Path.Combine (directory.FullName, fileName);
		}

		public override DBFolder GetItem (int index)
		{
			return _data [index];
		}

		public DBNote GetNote (int index)
		{
			DBFolder selectedFolder = this.SelectedFolder;
			if (selectedFolder != null) {
				return selectedFolder.Notes [index];
			}
			return null;
		}

		public void InsertFolder (string title)
		{
			DBFolder newFolder = new DBFolder (title);
			if (newFolder.InsertFolder ()) {
				this.LoadData ();
			}
		}

		public void DeleteFolder (int id)
		{
			DBFolder folder = this.FindObject<DBFolder> (id, _data);
			if (folder != null) {
				if (folder.DeleteFolder ()) {
					_data.Remove (folder);
					foreach (DBNote note in folder.Notes) {
						this.DeleteNoteResources (note.ID);
					}
				} else {
					this.LoadData ();
				}
			}
		}

		public void InsertNote ()
		{
			DBFolder selectedFolder = this.SelectedFolder;
			if (selectedFolder != null) {
				DBNote note = new DBNote (selectedFolder.ID);
				this.UnselectNotes (selectedFolder);
				note.Selected = true;
				selectedFolder.Count++;
				List<string> statements = this.AllStatements ();
				statements.Insert (0, note.InsertStatement ());
				bool success = DBObject.Save (statements.ToArray ());
				if (success) {
					selectedFolder.ResetNotes ();
					this.SendNewDataEvent ();
				} else {
					selectedFolder.Count--;
				}
			}
		}

		public void DeleteNote (int id)
		{
			DBFolder selectedFolder = this.SelectedFolder;
			if (selectedFolder != null) {
				DBNote deletedNote = this.FindNote (id);
				selectedFolder.Count--;
				bool success = this.UpdateSelectedFolder (deletedNote.DeleteStatement ());
				if (success) {
					this.SelectedFolder.Notes.Remove (deletedNote);
					this.SendNewDataEvent ();
					this.DeleteNoteResources (deletedNote.ID);
				} else {
					this.LoadData ();
					selectedFolder.Count++;
				}
			}
		}

		public void InsertNotePart (int index, int location, int length, string content, string type)
		{
			DBNote selectedNote = this.SelectedNote;
			if (selectedNote != null) {
				string data = this.CreateNotePart (content, type);
				if (type.Equals (DBNote.kTextCode)) {
					
					selectedNote.Content.Insert (index, data);

				} else if (type.Equals (DBNote.kImageCode) || type.Equals (DBNote.kVideoCode)) {
					nint newLocation = location;
					int newIndex = index;
					this.ChangeNotePart (ref newIndex, ref newLocation, length, "", selectedNote);
					int insertedIndex = newIndex;
					if (newLocation > 0) {
						string currentNotePart = selectedNote.Content [newIndex];
						string currentType = currentNotePart.Substring (0, DBNote.kTypeCodeLength);
						if (currentType == DBNote.kTextCode) {
							string currentContent = currentNotePart.Substring (DBNote.kTypeLength);
							if (newLocation < currentContent.Length) {
								this.SplitNotePart (newIndex, (int)newLocation, selectedNote);
							}
						}
						insertedIndex++;
					}

					selectedNote.Content.Insert (insertedIndex, data);

				} else {
					throw new Exception ("Unknown note part type");
				}
				this.NotePartDidChange ();
			}
		}

		public void ChangeNotePart (int index, nint location, nint length, string text)
		{
			DBNote note = this.SelectedNote;
			if (note != null) {
				if (this.ChangeNotePart (ref index, ref location, length, text, note)) {
					this.NotePartDidChange ();
				}
			}
		}

		public string GetMediaPath (string content, out float aspectRatio)
		{
			int pathStartIndex = content.IndexOf (Constants.AttachmentDivider);
			aspectRatio =  float.Parse (content.Substring (0, pathStartIndex));
			string path = content.Substring (pathStartIndex + 1);
			return path;
		}

		protected bool Load ()
		{
			GlobalDataContext.SharedInstance ().IncreaseActivityIndicatorCounter ();
			this.CheckActivityIndicatorCounter ();
			List<int> ids = new List<int> ();
			foreach (DBFolder folder in _data) {
				ids.Add (folder.ID);
			}
			List<DBFolder> newFolders = null;
			try {
				newFolders = DBFolder.Folders (ids.ToArray ());
				_data.InsertRange (_data.Count, newFolders);
			} finally {
				GlobalDataContext.SharedInstance ().DecreaseActivityIndicatorCounter ();
				this.CheckActivityIndicatorCounter ();
			}
			bool loaded = newFolders != null ? newFolders.Count > 0 : false;
			return loaded;
		}

		T SelectedObject<T> (T[] objects) where T : DBObject
		{
			int selectedIndex = 0;
			bool selected = objects.Any (item => {
				if (item.Selected) {
					return true;
				}
				selectedIndex++;
				return false;
			});
			if (selected) {
				return objects [selectedIndex];
			}
			return null;
		}

		string CreateNotePart (string content, string type)
		{
			string divider = "//";
			return type + divider + content;
		}

		void CheckActivityIndicatorCounter ()
		{
			this.SetLoading (GlobalDataContext.SharedInstance ().ActivityIndicatorCounter > 0);
		}

		void UnselectAll ()
		{
			this.UnselectFolders (folder => {
				this.UnselectNotes (folder);
			});
		}

		void UnselectFolders (Action<DBFolder> completion)
		{
			foreach (DBFolder folder in _data) {
				folder.Selected = false;
				if (completion != null) {
					completion (folder);
				}
			}
		}

		void UnselectNotes (DBFolder folder)
		{
			foreach (DBNote note in folder.Notes) {
				note.Selected = false;
			}
		}

		bool UpdateSelectedFolder (string noteStatement)
		{
			bool success = false;
			DBCommandsEngine engine = DBCommandsEngine.SharedInstance ();
			engine.ExecuteTransaction (connection => {
				string folderStatement = this.SelectedFolder.UpdateStatement ();
				success = engine.ExecuteNonQuery (connection, noteStatement) != Constants.NonQueryFailed &&
					engine.ExecuteNonQuery (connection, folderStatement) != Constants.NonQueryFailed;
			});
			return success;
		}

		void DeleteNoteResources (int noteID)
		{
			DirectoryInfo directory = Directory.CreateDirectory (
				Path.Combine (Constants.ResourcesDirectory, noteID.ToString ())
			);
			if (directory.Exists) {
				directory.Delete (true);
			}
		}

		DBNote FindNote (int id)
		{
			DBNote note = null;
			DBFolder selectedFolder = this.SelectedFolder;
			if (selectedFolder != null) {
				note = this.FindObject<DBNote> (id, selectedFolder.Notes);
			}
			return note;
		}

		T FindObject<T> (int id, List<T> objects) where T : DBObject
		{
			return objects.Find (item => {
				return item.ID == id;
			});
		}
			
		List<string> AllStatements ()
		{
			List<string> statements = new List<string> ();
			foreach (DBFolder folder in _data) {
				statements.Add (folder.UpdateStatement ());
				foreach (DBNote note in folder.Notes) {
					statements.Add (note.UpdateStatement ());
				}
			}
			return statements;
		}

		bool SaveAll ()
		{
			
			return DBObject.Save (this.AllStatements ().ToArray ());
		}

		bool ChangeNotePart (ref int index, ref nint location, nint length, string text, DBNote note)
		{
			if (index < note.Content.Count) {
				string notePart = note.Content [index];
				string insertedText = text;
				int contentIndex = (int)location + DBNote.kTypeLength;
				string type = notePart.Substring (0, DBNote.kTypeCodeLength);
				int remainderLength = type == DBNote.kTextCode ? 
					notePart.Length - contentIndex : Constants.AttachmentLength - (int)location;

				bool lessThanLength = remainderLength < length;
				int removedLength = this.GetNotePartRemovedLength (
					location, 
					length,
					remainderLength,
					lessThanLength,
					type,
					notePart
		        );

				int newIndex = index;
				int nextIndex = index + 1;
				int nextLocation = 0, newLocation = (int)location;
				this.ChangeContent (
					ref newIndex,
					note,
					ref nextIndex,
					ref nextLocation,
					notePart,
					type,
					contentIndex,
					ref newLocation,
					removedLength,
					insertedText
				);

				if (lessThanLength) {
					nint nativeNextLocation = nextLocation;
					this.ChangeNotePart (ref nextIndex, ref nativeNextLocation, length - remainderLength, "", note);
				}
				index = newIndex;
				location = newLocation;
				return true;
			}
			return false;
		}

		void ChangeContent (
			ref int newIndex,
			DBNote note, 
			ref int nextIndex, 
			ref int nextLocation, 
			string notePart,
			string type,
			int contentIndex,
			ref int location,
			int removedLength,
			string insertedText
		)
		{
			string changedNotePart = notePart.Remove (contentIndex, removedLength);

			changedNotePart = this.InsertText (
				changedNotePart, 
				contentIndex,
				location,
				insertedText, 
				notePart, 
				type, 
				removedLength,
				note,
				ref newIndex,
				ref nextIndex,
				ref nextLocation
			);
			int previousContentLength = this.ReplaceOldContent (
				newIndex,
				note,
				ref nextIndex,
				ref nextLocation,
				notePart,
				type,
				changedNotePart
			);
			location += previousContentLength;
			if (previousContentLength > 0) {
				newIndex--;
			}
		}

		int ReplaceOldContent (
			int newIndex,
			DBNote note, 
			ref int nextIndex, 
			ref int nextLocation, 
			string notePart,
			string type,
			string changedNotePart
		)
		{
			int previousContentLength;
			note.Content.RemoveAt (newIndex);
			if (changedNotePart.Length - DBNote.kTypeLength > 0) {
				note.Content.Insert (newIndex, changedNotePart);
				previousContentLength = this.CheckOnMutating (newIndex, note, ref nextIndex, ref nextLocation, changedNotePart, type);
			} else {
				nextIndex--;
				this.DeleteResources (notePart);
				previousContentLength = this.CheckOnMerge (newIndex, note, ref nextIndex, ref nextLocation);
			}
			return previousContentLength;
		}

		void SplitNotePart (int index, int location, DBNote note)
		{
			string part = note.Content [index];
			note.Content.Remove (part);
			int contentIndex = location + DBNote.kTypeLength;
			string firstPart = part.Substring (0, contentIndex);
			string typePart = part.Substring (0, DBNote.kTypeLength);
			string secondPart = typePart + part.Substring (contentIndex);
			note.Content.Insert (index, secondPart);
			note.Content.Insert (index, firstPart);
		}

		void MergeNoteParts (int firstIndex, DBNote note)
		{
			string firstPart = note.Content [firstIndex];
			string secondPart = note.Content [firstIndex + 1];
			note.Content.Remove (firstPart);
			note.Content.Remove (secondPart);
			string secondContent = secondPart.Substring (DBNote.kTypeLength);
			string mergedPart = firstPart.Insert (firstPart.Length, secondContent);
			note.Content.Insert (firstIndex, mergedPart);
		}

		void NotePartDidChange ()
		{
			DBNote selectedNote = this.SelectedNote;
			selectedNote.LastEdit = DateTime.Now;
			DBNote.Save (new [] {selectedNote.UpdateStatement ()});
			this.SendEvent (this.SelectedNoteDidChangeEvent);
		}

		int CheckOnMerge (int index, DBNote note, ref int nextIndex, ref int nextLocation)
		{
			if (index > 0 && index < note.Content.Count) {
				int previousIndex = index - 1;
				string previousPart = note.Content [previousIndex];
				string nextPart = note.Content [index];
				string previousType = previousPart.Substring (0, DBNote.kTypeCodeLength);
				string nextType = nextPart.Substring (0, DBNote.kTypeCodeLength);
				if (previousType == nextType && previousType == DBNote.kTextCode) {
					this.MergeNoteParts (previousIndex, note);
					string previousContent = previousPart.Substring (DBNote.kTypeLength);
					if (index == nextIndex) {
						nextLocation += previousContent.Length; 
					}
					nextIndex--;
					return previousContent.Length;
				}
			}
			return 0;
		}

		int CheckOnMutating (
			int index, 
			DBNote note, 
			ref int nextIndex, 
			ref int nextLocation, 
			string changedNotePart, 
			string type
		)
		{
			int previousContentLength = 0;
			string changedType = changedNotePart.Substring (0, DBNote.kTypeCodeLength);
			if (changedType != type) {
				previousContentLength = this.CheckOnMergeTwoSiblings (index, note, ref nextIndex, ref nextLocation);
			}
			return previousContentLength;
		}

		int CheckOnMergeTwoSiblings (
			int index, 
			DBNote note, 
			ref int nextIndex, 
			ref int nextLocation
		)
		{
			this.CheckOnMerge (index + 1, note, ref nextIndex, ref nextLocation);
			int previousContentLength = this.CheckOnMerge (index, note, ref nextIndex, ref nextLocation);
			return previousContentLength;
		}

		int GetNotePartRemovedLength (
			nint location, 
			nint length, 
			int remainderLength, 
			bool lessThanLength, 
			string type, 
			string notePart
		)
		{
			int removedLength = 0;
			if (type != DBNote.kTextCode) {
				if (length >= Constants.AttachmentLength) {
					removedLength = notePart.Length - DBNote.kTypeLength;
				}
			} else if (lessThanLength) {
				removedLength = remainderLength;
			} else {
				removedLength = (int)length;
			}
			return removedLength;
		}

		void DeleteResources (string notePart)
		{
			string type = notePart.Substring (0, DBNote.kTypeCodeLength);
			if (type != DBNote.kTextCode) {
				string content = notePart.Substring (DBNote.kTypeLength);
				float aspectRatio;
				string pathContent = this.GetMediaPath (content, out aspectRatio);
				string path = Path.Combine (Constants.ResourcesDirectory, pathContent);
				string previewPath = Path.ChangeExtension (path, Constants.PreviewImageExtension);
				string imagePath = Path.ChangeExtension (path, Constants.OriginImageExtension);
				string videoPath = Path.ChangeExtension (path, Constants.OriginMovieExtension);
				File.Delete (previewPath);
				File.Delete (imagePath);
				File.Delete (videoPath);
			}
		}

		string InsertText (string changedNotePart, int location, string insertedText)
		{
			changedNotePart = changedNotePart.Insert (location, insertedText);
			return changedNotePart;
		}

		string ReplaceMediaPart (string changedNotePart, int location, string insertedText, string notePart)
		{
			changedNotePart = this.InsertText (changedNotePart, location, insertedText);
			changedNotePart = changedNotePart.Remove (0, DBNote.kTypeCodeLength);
			changedNotePart = changedNotePart.Insert (0, DBNote.kTextCode);
			this.DeleteResources (notePart);
			return changedNotePart;
		}

		string InsertText (
			string changedNotePart, 
			int contentIndex,
			int originLocation,
			string insertedText, 
			string notePart, 
			string type, 
			int removedLength,
			DBNote note,
			ref int newIndex,
			ref int nextIndex,
			ref int nextLocation
		)
		{
			if (insertedText.Length > 0) {
				if (type == DBNote.kTextCode) {
					changedNotePart = this.InsertText (changedNotePart, contentIndex, insertedText);
				} else if (removedLength > 0) {
					changedNotePart = this.ReplaceMediaPart (changedNotePart, 0 + DBNote.kTypeLength, insertedText, notePart);
				} else {
					this.InsertTextPart (insertedText, originLocation, note, ref newIndex, ref nextIndex, ref nextLocation);
				}
			}
			return changedNotePart;
		}

		void InsertTextPart (
			string insertedText, 
			int location,
			DBNote note,
			ref int newIndex,
			ref int nextIndex,
			ref int nextLocation
		)
		{
			string data = this.CreateNotePart (insertedText, DBNote.kTextCode);
			int insertIndex, mergeIndex;
			bool insertBefore = location == 0;
			if (insertBefore) {
				insertIndex = mergeIndex = newIndex;
				newIndex++;
				nextIndex++;
			} else {
				insertIndex = newIndex + 1;
				mergeIndex = insertIndex + 1;
				if (nextIndex > newIndex) {
					nextIndex++;
				}
			}
			note.Content.Insert (insertIndex, data);
			int beforeMerge = nextIndex;
			this.CheckOnMerge (mergeIndex, note, ref nextIndex, ref nextLocation);
			int afterMerge = nextIndex;
			if (insertBefore) {
				newIndex -= beforeMerge - afterMerge;
			}
		}
	}
}

