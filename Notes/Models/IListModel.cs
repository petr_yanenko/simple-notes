﻿using System;

namespace Notes
{
	public interface IListModel<T>
	{
		int Count { get; }
		void SelectItem (int index);
		T GetItem (int index);
	}
}

