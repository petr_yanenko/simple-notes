﻿using System;

using Foundation;
using UIKit;

namespace Notes
{
	public abstract class EditableViewController : CustomStyleViewController
	{
		IEditableTableViewModel ViewModel { get { return (IEditableTableViewModel)_viewModel; } }
		IiOSViewModel IOSViewModel { get { return (IiOSViewModel)_viewModel; } }

		public EditableViewController () : base ()
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.NavigationItem.Title = this.IOSViewModel.NavigationTitle;

			this.NavigationItem.RightBarButtonItem = this.EditButtonItem;
		}

		public override void SetEditing (bool editing, bool animated)
		{
			base.SetEditing (editing, animated);
			_tableView.SetEditing (editing, animated);
			if (editing) {
				this.ViewModel.EditingDidStart ();
			} else {
				this.ViewModel.EditingDidEnd ();
			}
		}

		protected override void AddObservers ()
		{
			base.AddObservers ();
			this.ViewModel.DeletedPathEvent += (object sender) => {
				_tableView.DeleteRows (new[] { this.ViewModel.DeletedPath }, UITableViewRowAnimation.Automatic);
			};
			PropertyDidChangeHandler selectRowHandler = (object sender) => {
				_tableView.SelectRow (this.ViewModel.SelectedRowPath, true, UITableViewScrollPosition.Top);
			};
			this.ViewModel.SelectRowEvent += selectRowHandler;
			this.ViewModel.HighlightSelectedRowEvent += (object sender) => {
				if (this.TraitCollection.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
					selectRowHandler (this);
				}
			};
			this.ViewModel.MoveRowEvent += (object sender) => {
				_tableView.MoveRow (this.ViewModel.FromIndexPath, this.ViewModel.ToIndexPath);
			};
			this.ViewModel.ReloadRowEvent += (object sender) => {
				_tableView.ReloadRows (new [] {this.ViewModel.ReloadIndexPath}, UITableViewRowAnimation.Automatic);
			};
		}

		protected class EditableDataSource : CustomStyleSource
		{
			EditableViewController ParentController { get { return (EditableViewController)_parentController; } }

			public EditableDataSource (EditableViewController controller) : base (controller) {}

			protected override UITableViewCell CreateCell (UITableView tableView, NSIndexPath indexPath)
			{
				return new UITableViewCell (
					UITableViewCellStyle.Subtitle, 
					this.ParentController.ViewModel.CellReuseIdentifier (null)
				);
			}

			protected override void ConfigureCell (UITableViewCell cell, NSIndexPath indexPath)
			{
				base.ConfigureCell (cell, indexPath);
				SubtitleCellModel cellModel = this.ParentController.ViewModel.SubtitleCellViewModel (indexPath);
				cell.TextLabel.Text = cellModel.Title;
				cell.DetailTextLabel.Text = cellModel.Subtitle;
				if (this.ParentController.TraitCollection.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad && cellModel.Selected) {
					this.ParentController._tableView.SelectRow (indexPath, true, UITableViewScrollPosition.None);
				}
			}

			public override void CommitEditingStyle (
				UITableView tableView, 
				UITableViewCellEditingStyle editingStyle, 
				NSIndexPath indexPath
			)
			{
				this.ParentController.ViewModel.DeleteRowAction (indexPath);
			}

			public override void DidEndEditing (UITableView tableView, NSIndexPath indexPath)
			{
				this.ParentController.ViewModel.EditingDidEnd ();
			}
		}
	}
}

