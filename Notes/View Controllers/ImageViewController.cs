﻿using System;

using Foundation;
using UIKit;
using CoreGraphics;

namespace Notes
{
	public class ImageViewController : UIViewController
	{
		string _path = null;
		UIImage _image = null;

		UIScrollView ScrollView { get; set; }
		UIImageView ImageView { get; set; }
		UIToolbar Toolbar { get; set; }

		public ImageViewController (string path) : base ()
		{
			_path = path;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			CoreFoundation.DispatchQueue.DefaultGlobalQueue.DispatchAsync (() => {
				NSData imageData = NSData.FromFile (_path);
				CoreFoundation.DispatchQueue.MainQueue.DispatchAsync (() => {
					_image = new UIImage (imageData);
					this.ConfigureImageView();
				});
			});
			this.View.BackgroundColor = UIColor.Black;
			this.Toolbar = this.CreateToolbar ("Done", DoneAction);
			this.ScrollView = new UIScrollView ();
			this.ScrollView.Delegate = new ScrollViewDelegate (this);
			this.View.AddConstraintsSubview (this.ScrollView);
			this.View.AddConstraintMargin (NSLayoutAttribute.Top, this.ScrollView);
			this.View.AddConstraintMargin (NSLayoutAttribute.Leading, this.ScrollView);
			this.View.AddConstraintMargin (NSLayoutAttribute.Trailing, this.ScrollView);
			this.View.AddConstraintMargin (this.Toolbar, NSLayoutAttribute.Top, this.ScrollView,  NSLayoutAttribute.Bottom);
			this.View.AddConstraintMargin (NSLayoutAttribute.Leading, this.Toolbar);
			this.View.AddConstraintMargin (NSLayoutAttribute.Trailing, this.Toolbar);
			this.View.AddConstraintMargin (NSLayoutAttribute.Bottom, this.Toolbar);
		}

		public override void ViewWillTransitionToSize (CGSize toSize, IUIViewControllerTransitionCoordinator coordinator)
		{
			this.ConfigureZoom ();
		}

		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();
			this.ConfigureZoom ();
		}

		void ConfigureImageView ()
		{
			if (this.PresentingViewController != null) {
				this.ImageView = new UIImageView (_image);
				this.ScrollView.AddConstraintsSubview (this.ImageView);
				this.ScrollView.AddConstraintMargin (NSLayoutAttribute.Top, this.ImageView);
				this.ScrollView.AddConstraintMargin (NSLayoutAttribute.Leading, this.ImageView);
				this.ScrollView.AddConstraintMargin (NSLayoutAttribute.Trailing, this.ImageView);
				this.ScrollView.AddConstraintMargin (NSLayoutAttribute.Bottom, this.ImageView);

				this.ConfigureZoom ();
			}
		}

		void ConfigureZoom ()
		{
			if (_image != null) {
				double minZoom = Math.Min (
					this.ScrollView.Bounds.Width / _image.Size.Width, 
					this.ScrollView.Bounds.Height / _image.Size.Height
				);
				minZoom = Math.Min (minZoom, 1);
				this.ScrollView.MinimumZoomScale = (nfloat)minZoom;
				this.ScrollView.MaximumZoomScale = 2f;
				this.ScrollView.ZoomScale = this.ScrollView.MinimumZoomScale;
			}
		}

		void DoneAction (object sender, EventArgs args)
		{
			this.DismissViewControllerAsync (true);
		}

		class ScrollViewDelegate : UIScrollViewDelegate
		{
			ImageViewController _parentController;

			public ScrollViewDelegate (ImageViewController parent) : base ()
			{
				_parentController = parent;
			}

			public override UIView ViewForZoomingInScrollView (UIScrollView scrollView)
			{
				return _parentController.ImageView;
			}

			public override void DidZoom (UIScrollView scrollView)
			{
				CGSize contentSize = scrollView.ContentSize;
				CGRect bounds = scrollView.Bounds;
				UIEdgeInsets inset = UIEdgeInsets.Zero;
				if (bounds.Width > contentSize.Width) {
					inset.Left = (bounds.Width - contentSize.Width) / 2;
				}
				if (bounds.Height > contentSize.Height) {
					inset.Top = (bounds.Height - contentSize.Height) / 2;
				}
				scrollView.ContentInset = inset;
			}
		}
	}
}

