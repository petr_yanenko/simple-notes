﻿using System;

using UIKit;

namespace Notes
{
	public class ContainerViewController : UISplitViewController
	{
		public ContainerViewController () : base ()
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.SetNeedsStatusBarAppearanceUpdate ();
			this.Delegate = new SplitViewControllerDelegate ();
		}

		public override UIStatusBarStyle PreferredStatusBarStyle ()
		{
			return UIStatusBarStyle.LightContent;
		}

		protected class SplitViewControllerDelegate : UISplitViewControllerDelegate
		{
			public override bool CollapseSecondViewController (
				UISplitViewController splitViewController, 
				UIViewController secondaryViewController, 
				UIViewController primaryViewController
			)
			{
				return true;
			}
		}
	}
}

