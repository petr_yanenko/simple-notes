﻿using System;
using System.Collections.Generic;
using System.Linq;

using UIKit;
using Foundation;
using CoreGraphics;
using MobileCoreServices;
using AVKit;
using AVFoundation;

namespace Notes
{
	public class ContentViewController : BaseViewController
	{
		TapableAttachmentTextView _textView = null;
		UIToolbar _toolbar = null;
		NSLayoutConstraint _bottomConstraint = null;

		ContentViewModel ViewModel { get { return (ContentViewModel)_viewModel; } }

		~ContentViewController ()
		{
		}

		public ContentViewController () : base ()
		{
			_viewModel = new ContentViewModel (NotesModel.SharedInstance ());
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.ViewModel.SplitViewContainerBounds = this.SplitViewController.View.Bounds;
			this.ViewModel.IsPhone = this.TraitCollection.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone;
			_textView.TextAlignment = UITextAlignment.Left;
			_textView.AttributedText = this.ViewModel.Content ();
			_textView.AlwaysBounceVertical = true;
			_textView.KeyboardDismissMode = UIScrollViewKeyboardDismissMode.Interactive;
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillShowNotification, notification => {
				CGRect keyboardRect = UIKeyboard.FrameEndFromNotification (notification);
				double duration = UIKeyboard.AnimationDurationFromNotification (notification);
//				double curve = UIKeyboard.AnimationCurveFromNotification (notification);
				CGRect windowRect = _toolbar.ConvertRectToView (_toolbar.Bounds, null);
				nfloat keyboardHeight = keyboardRect.Height;
				nfloat constant = 
					- keyboardHeight + (
						UIApplication.SharedApplication.KeyWindow.Bounds.Height - windowRect.GetMaxY () + _bottomConstraint.Constant
					);
				this.PerformKeyboardAnimation (constant, duration);
			});
			NSNotificationCenter.DefaultCenter.AddObserver (UIKeyboard.WillHideNotification, notification => {
				double duration = UIKeyboard.AnimationDurationFromNotification (notification);
				this.PerformKeyboardAnimation (0, duration);
			});
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			this.SetContent ();
		}

		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();
			this.ViewModel.TextViewBounds = _textView.Bounds;
		}

		public override void ViewWillTransitionToSize (CGSize toSize, IUIViewControllerTransitionCoordinator coordinator)
		{
			this.ViewModel.RotateAction ();
		}

		void PerformKeyboardAnimation (nfloat constant, double duration)
		{
			this.View.LayoutIfNeeded ();
			_bottomConstraint.Constant = constant;
			UIView.Animate (duration, 0, UIViewAnimationOptions.CurveLinear, () => {
				this.View.LayoutIfNeeded ();
			}, null);
		}

		protected override void CreateScreenContent ()
		{
			_textView = new TapableAttachmentTextView ();

			//Del button pressing does not generate this event
			_textView.ShouldChangeText = (UITextView textView, NSRange range, String text) => {
				return this.ViewModel.ShouldChangeContent (textView.AttributedText, range, text);
			};
			_textView.SelectionChanged += (object sender, EventArgs e) => {
				this.ViewModel.SelectedRange = _textView.SelectedRange;
			};

			_textView.AttachmentCharacterIndexEvent += (object sender) => {
				this.ViewModel.AttachmentTapAction (_textView.AttributedText, _textView.AttachmentCharacterIndex);
			};

			this.View.AddConstraintsSubview (_textView);
			this.View.AddConstraintMargin (this.TopLayoutGuide, NSLayoutAttribute.Bottom, _textView, NSLayoutAttribute.Top);
			this.View.AddConstraintMargin (NSLayoutAttribute.Leading, _textView);
			this.View.AddConstraintMargin (NSLayoutAttribute.Trailing, _textView);

			UIBarButtonItem newNoteItem = new UIBarButtonItem (
				Constants.NewNoteTitle,
				UIBarButtonItemStyle.Bordered,
				NewNoteAction
			);
			this.NavigationItem.RightBarButtonItem = newNoteItem;

			_toolbar = this.CreateToolbar ("Add", AddAttachmentAction);
			this.View.AddConstraintMargin (_textView, NSLayoutAttribute.Bottom, _toolbar, NSLayoutAttribute.Top);
			this.View.AddConstraintMargin (NSLayoutAttribute.Leading, _toolbar);
			this.View.AddConstraintMargin (NSLayoutAttribute.Trailing, _toolbar);
			_bottomConstraint = this.View.AddConstraintMargin (this.BottomLayoutGuide, NSLayoutAttribute.Top, _toolbar, NSLayoutAttribute.Bottom);
		}

		protected override void AddObservers ()
		{
			this.HandleNewDataEvents ();
			this.HandleAddingAtachmentEvents ();
			this.HandleAttachmentEvents ();
		}

		void HandleNewDataEvents ()
		{
			this.ViewModel.NewDataEvent += (object sender) => {
				this.SetContent ();
			};
			this.ViewModel.EnabledEvent += (object sender) => {
				_textView.UserInteractionEnabled = this.ViewModel.Enabled;
			};
		}
			
		void HandleAddingAtachmentEvents ()
		{
			this.ViewModel.ShowAttachmentDialogEvent += (object sender) => {
				if (this.ViewModel.ShowAttachmentDialog) {
					UIAlertController alert = UIAlertController.Create (
						NSBundle.MainBundle.LocalizedString ("Choose Attachment", null),
						null,
						UIAlertControllerStyle.ActionSheet
					);
					this.CheckInterfaceIdiom (alert);
					this.AddAttachemntButtons (alert);

					this.PresentViewControllerAsync (alert, true);
				}
			};

			this.ViewModel.ShowImagePikerEvent += (object sender) => {
				if (this.ViewModel.ShowImagePiker) {
					UIImagePickerController picker = this.CreatePickerController (this.ViewModel.PickerType);

					if (picker != null) {
						this.PresentViewControllerAsync (picker, true);
					} else {
						this.ViewModel.CancelImagePickerShowingAction ();
					}
				} else {
					this.DismissViewControllerAsync (true);
				}
			};
		}

		void HandleAttachmentEvents ()
		{
			this.ViewModel.ShowOriginVideoEvent += (object sender) => {
				string path = this.ViewModel.AttachmentPath;
				AVPlayerViewController movieViewController = new AVPlayerViewController ();
				AVPlayer player = new AVPlayer (NSUrl.FromFilename (path));
				movieViewController.Player = player;
				this.PresentViewControllerAsync (movieViewController, true);
			};
			this.ViewModel.ShowOriginImageEvent += (object sender) => {
				ImageViewController imageController = new ImageViewController (this.ViewModel.AttachmentPath);
				this.PresentViewControllerAsync (imageController, true);
			};
		}

		void SetContent ()
		{
			if (this.View.Window != null) {
				NSRange selectedRange = this.ViewModel.SelectedRange;
				int changedLength = this.ViewModel.ChangedLength;
				_textView.AttributedText = this.ViewModel.Content ();
				NSRange newSelectedRange = new NSRange (selectedRange.Location + changedLength, 0);
				_textView.SelectedRange = newSelectedRange;
				if (_textView.IsFirstResponder) {
					_textView.ScrollRangeToVisible (newSelectedRange);
				}
			}
		}

		void CheckInterfaceIdiom (UIViewController controller)
		{
			if (this.TraitCollection.UserInterfaceIdiom == UIUserInterfaceIdiom.Pad) {
				controller.ModalPresentationStyle = UIModalPresentationStyle.Popover;
				controller.PopoverPresentationController.BarButtonItem = _toolbar.Items.Last ();
			}
		}

		void AddAttachemntButtons (UIAlertController alert)
		{
			UIAlertAction libraryAction = UIAlertAction.Create (
				NSBundle.MainBundle.LocalizedString ("Photo Library", null),
				UIAlertActionStyle.Default,
				alertAction => {
					this.ViewModel.PhotoLibraryAction ();
				}
			);
			alert.AddAction (libraryAction);
			UIAlertAction cameraAction = UIAlertAction.Create (
				NSBundle.MainBundle.LocalizedString ("Camera", null),
				UIAlertActionStyle.Default,
				alertAction => {
					this.ViewModel.CameraAction ();
				}
			);
			alert.AddAction (cameraAction);
			UIAlertAction cancelAction = UIAlertAction.Create (
				NSBundle.MainBundle.LocalizedString ("Cancel", null),
				UIAlertActionStyle.Cancel,
				alertAction => {
					this.ViewModel.CancelPickerDialogAction ();
				}
			);
			alert.AddAction (cancelAction);
		}

		UIImagePickerController CreatePickerController (UIImagePickerControllerSourceType type)
		{
			if (UIImagePickerController.IsSourceTypeAvailable (type)) {
				UIImagePickerController picker = new UIImagePickerController ();
				this.CheckInterfaceIdiom (picker);
				picker.SourceType = type;
				List<string> cameraMediaTypes = new List<string> ();
				cameraMediaTypes.Add (UTType.Image);
				string[] mediaTypes = UIImagePickerController.AvailableMediaTypes (type);
				mediaTypes.Any<string> (item => {
					if (item.Equals (UTType.Movie)) {
						cameraMediaTypes.Add (UTType.Movie);
						return true;
					}
					return false;
				});
				picker.MediaTypes = cameraMediaTypes.ToArray ();

				picker.FinishedPickingMedia += (object sender, UIImagePickerMediaPickedEventArgs e) => {
					this.HandleFinishedPickingMedia (sender, e);
				};

				picker.Canceled += (object sender, EventArgs e) => {
					this.ViewModel.DismissImagePickerAction ();
				};

				return picker;
			}
			return null;
		}

		void AddAttachmentAction (object sender, EventArgs args)
		{
			this.ViewModel.AddAttachmentAction ();
		}

		void NewNoteAction (object sender, EventArgs args)
		{
			this.ViewModel.CreateNewNote ();
		}

		void HandleFinishedPickingMedia (object sender, UIImagePickerMediaPickedEventArgs e)
		{
			// determine what was selected, video or image
			bool isImage = false;
			switch(e.Info[UIImagePickerController.MediaType].ToString()) {
			case "public.image":
				isImage = true;
				break;
			case "public.video":
				break;
			}

//			NSUrl referenceURL = e.Info[new NSString("UIImagePickerControllerReferenceURL")] as NSUrl;

			// if it was an image, get the other image info
			if(isImage) {
				// get the original image
				UIImage originalImage = e.Info[UIImagePickerController.OriginalImage] as UIImage;
				if(originalImage != null) {
					// do something with the image
					this.ViewModel.InsertImageAttachmentAction (
						originalImage, _textView.AttributedText, _textView.SelectedRange
					);
				}
			} else { // if it's a video
				// get video url
				NSUrl mediaURL = e.Info[UIImagePickerController.MediaURL] as NSUrl;
				if(mediaURL != null) {
					this.ViewModel.InsertMovieAttachmentAction (mediaURL.Path, _textView.AttributedText, _textView.SelectedRange);
				}
			}
		}
	}
}

