﻿using System;

using UIKit;

namespace Notes
{
	public abstract class RefreshableTableViewController : CustomTableViewController
	{
		protected UIRefreshControl _refreshControl;

		public RefreshableTableViewController () : base () {}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			this.ReloadTable ();
		}

		protected override void ConfigureTableView ()
		{
			base.ConfigureTableView ();
			_refreshControl = new UIRefreshControl ();
			_refreshControl.TintColor = UIColor.FromRGB (0.635f, 0.141f, 0.247f);
			_refreshControl.ValueChanged += (object sender, EventArgs e) => {
				this.Refresh (_refreshControl);
			};
			_tableView.AddSubview (_refreshControl);
		}

		void Refresh (UIRefreshControl refresh)
		{
			this.ReloadTable ();
			_refreshControl.EndRefreshing ();
		}

		void ReloadTable () 
		{
			_viewModel.ReloadAction ();
		}

		void LoadTableData () 
		{
			_viewModel.LoadDataAction ();
		}

		protected abstract class RefreshableTableSource : CustomTableSource
		{
			RefreshableTableViewController ParentController { get { return (RefreshableTableViewController)_parentController; } }

			public RefreshableTableSource (RefreshableTableViewController controller) : base (controller) {}

			public override void DecelerationEnded (UIScrollView scrollView)
			{
				if (scrollView.ContentSize.Height - scrollView.ContentOffset.Y <= scrollView.Bounds.Size.Height) {
					this.ParentController.LoadTableData ();
				}
			}
		}
	}
}

