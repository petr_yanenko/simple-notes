﻿using System;

using Foundation;
using UIKit;

namespace Notes
{
	public abstract class CustomStyleViewController : RefreshableTableViewController
	{
		public CustomStyleViewController () : base () {}

		protected override void ConfigureTableView ()
		{
			base.ConfigureTableView ();
			_tableView.CellLayoutMarginsFollowReadableWidth = false;
		}

		protected abstract class CustomStyleSource : RefreshableTableSource
		{
			CustomStyleViewController ParentController { get { return (CustomStyleViewController)_parentController; } }

			public CustomStyleSource (CustomStyleViewController controller) : base (controller) {}

			protected override void ConfigureCell (UITableViewCell cell, NSIndexPath indexPath)
			{
				cell.SelectionStyle = UITableViewCellSelectionStyle.Default;
				cell.PreservesSuperviewLayoutMargins = false;
			}

			public override void WillDisplay (UITableView tableView, UITableViewCell cell, NSIndexPath indexPath) 
			{
				cell.SeparatorInset = UIEdgeInsets.Zero;
				cell.PreservesSuperviewLayoutMargins = false;
				cell.LayoutMargins = UIEdgeInsets.Zero;
			}
		}
	}
}

