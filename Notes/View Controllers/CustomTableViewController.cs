﻿

using System;

using UIKit;
using Foundation;
using ObjCRuntime;

namespace Notes
{
	public abstract class CustomTableViewController : BaseViewController
	{
		protected UITableView _tableView = null;

		ITableViewModel ViewModel { get { return (ITableViewModel)_viewModel; } }

		public CustomTableViewController () : base () {}

		protected abstract UITableViewSource CreateTableSource ();

		~CustomTableViewController ()
		{
			CoreFoundation.DispatchQueue.MainQueue.DispatchSync (() => {
				if (_tableView != null) {
					_tableView.Source = null;
				}
			});
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			if (this.TraitCollection.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone) {
				NSIndexPath[] indexPaths = _tableView.IndexPathsForSelectedRows;
				if (indexPaths != null) {
					foreach (NSIndexPath path in indexPaths) {
						_tableView.DeselectRow (path, true);
						try {
							_tableView.Source.RowDeselected (_tableView, path);
						} catch (Exception) {
						}
					}
				}
			}
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			_tableView.FlashScrollIndicators ();
		}

		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();
			_tableView.ReloadData ();
		}

		protected override void AddObservers ()
		{
			_viewModel.NewDataEvent += (object sender) => {
				_tableView.ReloadData ();
			};
		}

		protected override void CreateScreenContent ()
		{
			this.CreateTableView ();
			this.AddConstraints ();
			this.ConfigureTableView ();
		}

		protected virtual void CreateTableView ()
		{
			_tableView = new UITableView ();
			this.View.AddConstraintsSubview (_tableView);
			_tableView.Source = this.CreateTableSource ();
		}

		protected virtual void AddConstraints ()
		{
			this.View.AddConstraintMargin (this.TopLayoutGuide, NSLayoutAttribute.Bottom, _tableView, NSLayoutAttribute.Top);
			this.View.AddConstraintMargin (this.BottomLayoutGuide, NSLayoutAttribute.Top, _tableView, NSLayoutAttribute.Bottom);
			this.View.AddConstraintMargin (NSLayoutAttribute.Leading, _tableView);
			this.View.AddConstraintMargin (NSLayoutAttribute.Trailing, _tableView);
		}

		protected virtual void ConfigureTableView ()
		{
			_tableView.TableFooterView = new UIView ();
		}

		protected abstract class CustomTableSource : UITableViewSource
		{
			protected CustomTableViewController _parentController = null;

			protected abstract void ConfigureCell (UITableViewCell cell, NSIndexPath indexPath);

			public CustomTableSource (CustomTableViewController controller)
			{
				_parentController = controller;
			}

			public override nint RowsInSection (UITableView tableView, nint section)
			{
				return _parentController.ViewModel.CellsNumber (section);
			}

			public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
			{
				UITableViewCell cell = tableView.DequeueReusableCell (_parentController.ViewModel.CellReuseIdentifier (indexPath));
				if (cell == null) {
					cell = this.CreateCell (tableView, indexPath);
				}

				this.ConfigureCell (cell, indexPath);

				return cell;
			}

			public override nfloat GetHeightForRow (UITableView tableView, NSIndexPath indexPath)
			{
				return _parentController.ViewModel.RowHeight (tableView.Bounds.Size.Height, indexPath);
			}

			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
				_parentController.ViewModel.SelectRowAction (indexPath);
			}

			protected virtual UITableViewCell CreateCell (UITableView tableView, NSIndexPath indexPath)
			{
				return null;
			}
		}
	}
}

