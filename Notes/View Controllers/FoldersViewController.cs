﻿using System;

using UIKit;
using Foundation;

namespace Notes
{
	public class FoldersViewController : EditableViewController
	{
		UIToolbar _toolbar = null;

		FoldersViewModel ViewModel { get { return (FoldersViewModel)_viewModel; } }

		public FoldersViewController () : base ()
		{
			_viewModel = new FoldersViewModel (NotesModel.SharedInstance ());
		}

		protected override void CreateScreenContent ()
		{
			this.CreateToolbar ();
			base.CreateScreenContent ();
		}

		void CreateToolbar ()
		{
			_toolbar = this.CreateToolbar ("New Folder", NewFolderAction);
		}

		void NewFolderAction (object sender, EventArgs args)
		{
			this.ViewModel.InsertFolderAction ();
		}

		protected override void AddConstraints ()
		{
			this.View.AddConstraintMargin (this.TopLayoutGuide, NSLayoutAttribute.Bottom, _tableView, NSLayoutAttribute.Top);
			this.View.AddConstraintMargin (NSLayoutAttribute.Leading, _tableView);
			this.View.AddConstraintMargin (NSLayoutAttribute.Trailing, _tableView);
			this.View.AddConstraintMargin (_tableView, NSLayoutAttribute.Bottom, _toolbar, NSLayoutAttribute.Top);
			this.View.AddConstraintMargin (NSLayoutAttribute.Leading, _toolbar);
			this.View.AddConstraintMargin (NSLayoutAttribute.Trailing, _toolbar);
			this.View.AddConstraintMargin (this.BottomLayoutGuide, NSLayoutAttribute.Top, _toolbar, NSLayoutAttribute.Bottom);
		}

		protected override void AddObservers ()
		{
			base.AddObservers ();
			this.ViewModel.ShowInsertDialogEvent += (object sender) => {
				if (this.ViewModel.ShowInsertDialog) {
					UIAlertController alert = UIAlertController.Create (
						NSBundle.MainBundle.LocalizedString ("New Folder", null),
						NSBundle.MainBundle.LocalizedString ("Enter a title for this folder", null),
						UIAlertControllerStyle.Alert
					);
					alert.AddTextField (nameField => {
						nameField.Placeholder = NSBundle.MainBundle.LocalizedString ("Name", null);
						nameField.ReturnKeyType = UIReturnKeyType.Done;
						Action<UIAlertAction> saveHandler = action => {
							this.ViewModel.SaveInsertedFolderAction (nameField.Text);
						};
						Action<UIAlertAction> cancelHandler = action => {
							this.ViewModel.CancelInsertFolderAction ();
						};
						UIAlertAction cancel = UIAlertAction.Create (
							NSBundle.MainBundle.LocalizedString ("Cancel", null),
							UIAlertActionStyle.Cancel,
							cancelHandler
						);
						alert.AddAction (cancel);
						UIAlertAction save = UIAlertAction.Create (
							NSBundle.MainBundle.LocalizedString ("Save", null),
							UIAlertActionStyle.Default,
							saveHandler
						);
						alert.AddAction (save);
					});
					this.PresentViewControllerAsync (alert, true);
				}
			};
			this.ViewModel.SelectRowEvent += (object sender) => {
				UIViewController nextController;
				if (this.TraitCollection.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone) {
					nextController = new CollapsedNotesViewController ();
				} else {
					nextController = new NotesViewController ();
				}
				this.NavigationController.PushViewController (nextController, true);
			};
		}

		protected override UITableViewSource CreateTableSource ()
		{
			return new FoldersDataSource (this);
		}

		protected class FoldersDataSource : EditableDataSource
		{
			FoldersViewController ParentController { get { return (FoldersViewController)_parentController; } }

			public FoldersDataSource (FoldersViewController controller) : base (controller) {}
		}
	}
}

