﻿using System;

using UIKit;
using Foundation;

namespace Notes
{
	public class NavigationController : UINavigationController
	{

		public NavigationController (UIViewController rootViewController) : base (rootViewController) {}
		public NavigationController () : base () {}
		
		~ NavigationController ()
		{
			CoreFoundation.DispatchQueue.MainQueue.DispatchSync (() => {
				if (this.InteractivePopGestureRecognizer != null) {
					this.InteractivePopGestureRecognizer.WeakDelegate = null;
				}
			});
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			UIStringAttributes attributes = new UIStringAttributes ();
			attributes.ForegroundColor = Constants.MainTextColor;
			attributes.Font = Constants.NavigationBarFont;
			this.NavigationBar.TitleTextAttributes = attributes;

			UIColor mainColor = Constants.MainBackgroundColor;

			this.View.BackgroundColor = mainColor;
			this.NavigationBar.BarTintColor = mainColor;
			this.NavigationBar.TintColor = Constants.MainTintColor;
			this.NavigationBar.SetBackgroundImage (new UIImage (), UIBarMetrics.Default);
			this.NavigationBar.ShadowImage = new UIImage ();
			this.NavigationBar.Translucent = false;

			this.InteractivePopGestureRecognizer.WeakDelegate = this;
		}

		public override UIStatusBarStyle PreferredStatusBarStyle ()
		{
			return UIStatusBarStyle.LightContent;
		}
	}
}

