﻿using System;

using Foundation;
using UIKit;

namespace Notes
{
	public class CollapsedNotesViewController : NotesViewController
	{
		public CollapsedNotesViewController () : base ()
		{
		}

		protected override void AddObservers ()
		{
			base.AddObservers ();
			this.ViewModel.SelectRowEvent += (object sender) => {
				this.SplitViewController.ShowDetailViewController (new ContentViewController (), this);
			};
		}
	}
}

