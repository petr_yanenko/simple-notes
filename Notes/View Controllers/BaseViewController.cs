﻿using System;

using UIKit;

namespace Notes
{
	public abstract class BaseViewController : UIViewController
	{
		protected IViewModel _viewModel;
		protected IRouter _router;

		~BaseViewController ()
		{
			CoreFoundation.DispatchQueue.MainQueue.DispatchSync (() => {
				_viewModel.CancelAction ();
			});
		}
		public BaseViewController () : base () {}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.
			this.AddObservers ();
			this.CreateScreenContent ();
		}

		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			if (this.IsBeingDismissed || this.IsMovingFromParentViewController) {
				_viewModel.CancelAction ();
			}
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}

		protected abstract void AddObservers ();

		protected abstract void CreateScreenContent ();
	}
}


