﻿using System;
using System.Collections;

using Foundation;
using UIKit;

namespace Notes
{
	public class NotesViewController : EditableViewController
	{
		protected NotesViewModel ViewModel { get { return (NotesViewModel)_viewModel; } }

		public NotesViewController () : base ()
		{
			_viewModel = new NotesViewModel (NotesModel.SharedInstance ());
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			UIBarButtonItem newNoteItem = new UIBarButtonItem (
				Constants.NewNoteTitle,
				UIBarButtonItemStyle.Bordered,
				NewNoteAction
			);
			UIBarButtonItem[] rightButtons = this.NavigationItem.RightBarButtonItems;
			Array.Resize (ref rightButtons, rightButtons.Length + 1);
			rightButtons [rightButtons.Length - 1] = newNoteItem;
			this.NavigationItem.RightBarButtonItems = rightButtons;
		}

		protected override UITableViewSource CreateTableSource ()
		{
			return new NotesDataSource (this);
		}

		void NewNoteAction (object sender, EventArgs args)
		{
			this.ViewModel.CreateNewNote ();
		}

		protected class NotesDataSource : EditableDataSource
		{
			NotesViewController ParentController { get { return (NotesViewController)_parentController; } }

			public NotesDataSource (NotesViewController controller) : base (controller) {}
		}
	}
}

