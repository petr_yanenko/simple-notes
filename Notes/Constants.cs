﻿using System;
using System.IO;

using UIKit;
using Foundation;

namespace Notes
{

	public delegate void PropertyDidChangeHandler (object sender);

	public static class Constants
	{
		public static string Documents { get; } = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
		public static string TempDirectory { get; } = Path.GetTempPath ();
		public static string ResourcesDirectory { get; } = Path.Combine (Constants.Documents, "Resources");

		public static string OriginImageExtension { get; } = "img";
		public static string OriginMovieExtension { get; } = "mov";
		public static string PreviewImageExtension { get; } = "prv";
		public static string AttachmentDivider { get; } = "/";

		public static UIFont NavigationBarFont { get; } = UIFont.FromName ("HelveticaNeue-Light", 17.0f);
		public static UIColor MainBackgroundColor { get; } = UIColor.DarkGray;
		public static UIColor MainTintColor { get; } = UIColor.White;
		public static UIColor MainTextColor { get; } = UIColor.White;

		public static string DateFormat { get; } = "MM/dd/yyyy HH:mm";

		public static string OkTitle { get; } = NSBundle.MainBundle.LocalizedString ("Ok", null);
		public static string NewNoteTitle { get; } = NSBundle.MainBundle.LocalizedString ("New", null);

		public static int NotSelected { get; } = -1;

		public static int AttachmentLength { get; } = " ".Length;

		public static int NonQueryFailed { get; } = -1;
	}
}

