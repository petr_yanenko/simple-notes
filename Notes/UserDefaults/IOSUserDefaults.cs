﻿using System;

using Foundation;

namespace Notes
{
	public class IOSUserDefaults : UserDefaults
	{		
		public override void Save ()
		{
			NSUserDefaults.StandardUserDefaults.Synchronize ();
		}
	}
}

