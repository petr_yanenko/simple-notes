﻿using System;

namespace Notes
{
	public abstract class UserDefaults
	{
		static UserDefaults _instance = null;

		static UserDefaults ()
		{
			_instance = new IOSUserDefaults ();
		}

		public static UserDefaults SharedInstance ()
		{
			return _instance;
		}

		public abstract void Save ();
	}
}

